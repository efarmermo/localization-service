package mobi.efarmer.i18n.util;

import mobi.efarmer.i18n.datamodel.entity.dataimport.SessionEntity;
import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;

public class XmlProcessorTest {

    @Test
    public void testReadXml() {
        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream("i18n.xml");
            SessionEntity session = XmlProcessor.readXml(is);
            Assert.assertFalse(session.getLocalizations().isEmpty());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

}