package mobi.efarmer.i18n.rest.converter;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Maxim Maximchuk
 *         date 07.02.2016.
 */
public class JSONObjectHttpMessageConverter extends AbstractJsonHttpMessageConverter<JSONObject> {
    @Override
    protected boolean supports(Class<?> clazz) {
        return clazz == JSONObject.class;
    }

    @Override
    protected JSONObject readInternal(Class<? extends JSONObject> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        try {
            return new JSONObject(readInputMessage(inputMessage));
        } catch (JSONException e) {
            throw new HttpMessageNotReadableException(e.getMessage(), e);
        }
    }

    @Override
    protected void writeInternal(JSONObject jsonObject, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        try (BufferedOutputStream os = new BufferedOutputStream(outputMessage.getBody())) {
            os.write(jsonObject.toString().getBytes("UTF-8"));
        }
    }
}
