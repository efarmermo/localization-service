package mobi.efarmer.i18n.rest.converter;

import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.exception.JsonException;
import org.json.JSONArray;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 06.02.2016.
 */
public class JsonDTOListHttpMessageConverter<T extends JsonDTO> extends AbstractJsonDTOHttpMessageConverter<List<T>> {

    @Override
    protected boolean supports(Class<?> clazz) {
        return List.class.isAssignableFrom(clazz);
    }

    @Override
    protected boolean canRead(MediaType mediaType) {
        return false;
    }

    @Override
    protected List<T> readInternal(Class<? extends List<T>> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        throw new UnsupportedOperationException("read is not supported");
    }

    @Override
    protected void writeInternal(List<T> jsonDTOs, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        try (BufferedOutputStream os = new BufferedOutputStream(outputMessage.getBody())) {
            JSONArray jsonArray = new JSONArray();
            for (JsonDTO jsonDTO: jsonDTOs) {
                jsonArray.put(jsonDTO.toJSON());
            }
            os.write(jsonArray.toString().getBytes("UTF-8"));
        } catch (JsonException e) {
            throw new HttpMessageNotWritableException(e.getMessage(), e);
        }
    }
}
