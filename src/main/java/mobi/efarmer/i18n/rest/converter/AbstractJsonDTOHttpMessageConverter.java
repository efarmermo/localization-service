package mobi.efarmer.i18n.rest.converter;

import com.maximchuk.json.JsonDTO;
import org.json.JSONObject;

/**
 * @author Maxim Maximchuk
 *         date 06.02.2016.
 */
public abstract class AbstractJsonDTOHttpMessageConverter<T> extends AbstractJsonHttpMessageConverter<T> {

    protected JsonDTO createJsonDTO(Class<? extends JsonDTO> clazz, JSONObject jsonObject) throws Exception {
        JsonDTO jsonDTO = clazz.newInstance();
        jsonDTO.settingFromJson(jsonObject);
        return jsonDTO;
    }
}
