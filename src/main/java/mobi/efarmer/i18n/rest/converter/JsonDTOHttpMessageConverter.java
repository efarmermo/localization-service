package mobi.efarmer.i18n.rest.converter;

import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.exception.JsonException;
import org.json.JSONObject;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Maxim Maximchuk
 *         date 06.02.2016.
 */
public class JsonDTOHttpMessageConverter extends AbstractJsonDTOHttpMessageConverter<JsonDTO> {

    @Override
    protected boolean supports(Class<?> clazz) {
        return JsonDTO.class.isAssignableFrom(clazz);
    }

    @Override
    protected JsonDTO readInternal(Class<? extends JsonDTO> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        String jsonString = readInputMessage(inputMessage);
        try {
            return createJsonDTO(clazz, new JSONObject(jsonString));
        } catch (Exception e) {
            throw new HttpMessageNotReadableException(e.getMessage(), e);
        }
    }

    @Override
    protected void writeInternal(JsonDTO jsonDTO, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        try (BufferedOutputStream os = new BufferedOutputStream(outputMessage.getBody())) {
            os.write(jsonDTO.toJSON().toString().getBytes("UTF-8"));
        } catch (JsonException e) {
            throw new HttpMessageNotWritableException(e.getMessage(), e);
        }
    }
}
