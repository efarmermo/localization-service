package mobi.efarmer.i18n.rest.converter;

import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.xml.AbstractXmlHttpMessageConverter;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Maxim Maximchuk
 *         date 28.07.2016.
 */
public class String2XmlHttpMessageConverter extends AbstractXmlHttpMessageConverter<String> {
    @Override
    protected String readFromSource(Class<? extends String> clazz, HttpHeaders headers, Source source) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void writeToResult(String s, HttpHeaders headers, Result result) throws IOException {
        if (result instanceof StreamResult) {
            try (BufferedOutputStream os = new BufferedOutputStream(((StreamResult)result).getOutputStream())) {
                os.write(s.getBytes());
            }
        }
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return String.class.isAssignableFrom(clazz);
    }
}
