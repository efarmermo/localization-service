package mobi.efarmer.i18n.rest.converter;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author Maxim Maximchuk
 *         date 07.02.2016.
 */
public abstract class AbstractJsonHttpMessageConverter<T> extends AbstractHttpMessageConverter<T> {

    public AbstractJsonHttpMessageConverter() {
        super(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON_UTF8);
    }

    protected String readInputMessage(HttpInputMessage inputMessage) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        try (InputStream is = inputMessage.getBody()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            while (reader.ready()) {
                stringBuilder.append(reader.readLine());
            }
            return stringBuilder.toString();
        }
    }
}
