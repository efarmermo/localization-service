package mobi.efarmer.i18n.rest.converter;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Maxim Maximchuk
 *         date 07.02.2016.
 */
public class JSONArrayHttpMessageConverter extends AbstractJsonHttpMessageConverter<JSONArray> {

    @Override
    protected boolean supports(Class<?> clazz) {
        return clazz == JSONArray.class;
    }

    @Override
    protected JSONArray readInternal(Class<? extends JSONArray> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        try {
            return new JSONArray(readInputMessage(inputMessage));
        } catch (JSONException e) {
            throw new HttpMessageNotReadableException(e.getMessage(), e);
        }
    }

    @Override
    protected void writeInternal(JSONArray jsonArray, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        try (BufferedOutputStream os = new BufferedOutputStream(outputMessage.getBody())) {
            os.write(jsonArray.toString().getBytes("UTF-8"));
        };
    }
}
