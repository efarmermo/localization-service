package mobi.efarmer.i18n.rest;

import com.maximchuk.json.exception.JsonException;
import mobi.efarmer.i18n.datamodel.dao.HeadUsageEntityDao;
import mobi.efarmer.i18n.datamodel.entity.HeadUsageEntity;
import mobi.efarmer.i18n.rest.exception.HttpException;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;

/**
 * Created by Maxim Maximchuk
 * date 12-Jan-16.
 */
@RestController
@RequestMapping("usage")
public class ClientUsageController extends BaseController {

    @Autowired
    private HeadUsageEntityDao headUsageEntityDao;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveUsage(@RequestBody JSONArray jsonArray) throws HttpException {
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                headUsageEntityDao.createIfNotExists(new HeadUsageEntity(jsonArray.getJSONObject(i)));
            }
            return ResponseEntity.accepted().build();
        } catch (JsonException e) {
            throw invalidJsonError(e);
        } catch (SQLException e) {
            HttpException ex = savingError(e);
            ex.addMintExtraData("method", "translate request");
            ex.addMintExtraData("data", jsonArray.toString());
            throw ex;
        } catch (Exception e) {
            throw internalError(e);
        }
    }

}
