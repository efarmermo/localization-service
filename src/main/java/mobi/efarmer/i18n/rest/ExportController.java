package mobi.efarmer.i18n.rest;

import mobi.efarmer.i18n.datamodel.dao.LocalizationEntityDao;
import mobi.efarmer.i18n.datamodel.entity.LocalizationEntity;
import mobi.efarmer.i18n.rest.exception.HttpException;
import mobi.efarmer.i18n.util.XmlProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 06.02.15.
 */
@RestController
@RequestMapping("export")
public class ExportController extends BaseController {

    @Autowired
    private LocalizationEntityDao localizationEntityDao;

    @RequestMapping(value = "i18n/xml/{tag}", produces = MediaType.APPLICATION_XML_VALUE)
    public String exportLocalizations(@PathVariable("tag") String tag,
                                      @RequestParam(value = "ver", required = false) String version)
            throws HttpException {
        try {
            List<LocalizationEntity> localizationList = version != null ?
                    localizationEntityDao.findAllActualUsed(tag, version + "%") :
                    localizationEntityDao.findAllActualByTag(tag);
            return new String(XmlProcessor.writeXml(tag, localizationList));
        } catch (Exception e) {
            throw internalError(e);
        }
    }

}
