package mobi.efarmer.i18n.rest;

import mobi.efarmer.i18n.datamodel.dao.UserEntityDao;
import mobi.efarmer.i18n.datamodel.entity.UserEntity;
import mobi.efarmer.i18n.rest.exception.HttpException;
import mobi.efarmer.i18n.rest.security.RoleEnum;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 03.02.15.
 */
@RestController
@RequestMapping("user")
public class UserController extends BaseController {

    private static final String USERNAME_FIELD = "username";
    private static final String ROLE_FIELD = "role";

    @Autowired
    private UserEntityDao userEntityDao;

    @RequestMapping(value = "all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserEntity> getUsers() throws HttpException {
        try {
            return userEntityDao.queryForAll();
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "role/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @RolesAllowed("root")
    public ResponseEntity addRole(@RequestBody JSONObject json) throws HttpException {
        try {
            ResponseEntity response;
            UserEntity user = userEntityDao.findUser(json.getString(USERNAME_FIELD));
            RoleEnum role = RoleEnum.valueOf(json.getString(ROLE_FIELD).toUpperCase());
            if (user != null) {
                if (!user.hasRole(role)) {
                    userEntityDao.addRole(user, role);
                    response = ResponseEntity.accepted().build();
                } else {
                    response = ResponseEntity.status(HttpStatus.NOT_MODIFIED).body("Role already exist");
                }
            } else {
                response = ResponseEntity.status(HttpStatus.NOT_MODIFIED).body("User does`t exist");
            }
            return response;
        } catch (JSONException e) {
            throw badRequestError(e);
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public UserEntity getRoles() throws HttpException {
        try {
            return getUser();
        } catch (Exception e) {
            throw internalError(e);
        }
    }

}
