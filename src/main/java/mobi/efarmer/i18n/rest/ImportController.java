package mobi.efarmer.i18n.rest;

import com.maximchuk.json.exception.JsonException;
import mobi.efarmer.i18n.datamodel.dao.LocalizationEntityDao;
import mobi.efarmer.i18n.datamodel.dao.dataimport.SessionEntityDao;
import mobi.efarmer.i18n.datamodel.entity.LocalizationEntity;
import mobi.efarmer.i18n.datamodel.entity.dataimport.SessionEntity;
import mobi.efarmer.i18n.rest.exception.BadRequestHttpException;
import mobi.efarmer.i18n.rest.exception.HttpException;
import mobi.efarmer.i18n.util.XmlParseException;
import mobi.efarmer.i18n.util.XmlProcessor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.security.RolesAllowed;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 18.02.15.
 */
@RestController
@RequestMapping("import")
public class ImportController extends BaseController {

    @Autowired
    private SessionEntityDao sessionEntityDao;

    @Autowired
    private LocalizationEntityDao localizationEntityDao;

    @RequestMapping(value = "sessions", produces = MediaType.APPLICATION_JSON_VALUE)
    public JSONArray getSessions() throws HttpException {
        try {
            List<SessionEntity> sessions = sessionEntityDao.getAll();
            JSONArray jsonArray = new JSONArray();
            for (SessionEntity session : sessions) {
                jsonArray.put(buildSessionJson(session));
            }
            return jsonArray;
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "session/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getSession(@PathVariable("id") Long id) throws HttpException {
        try {
            ResponseEntity response;
            SessionEntity session = sessionEntityDao.findById(id);
            if (session != null) {
                session.setLocalizations(localizationEntityDao.findByImportSession(session));
                response = ResponseEntity.ok(buildSessionJson(session));
            } else {
                response = sessionNotFound(id);
            }
            return response;
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "session/localization/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateLocalization(@RequestBody JSONObject json) throws HttpException {
        try {
            ResponseEntity response = null;
            Long sessionId = json.getLong("sessionId");
            SessionEntity session = sessionEntityDao.findById(sessionId);
            if (session != null) {
                LocalizationEntity localization = new LocalizationEntity();
                localization.settingFromJson(json);
                if (sessionEntityDao.updateLocalization(session, localization) != 0) {
                    response = ResponseEntity.ok().build();
                }
            } else {
                response = sessionNotFound(sessionId);
            }
            return response != null ? response : ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
        } catch (JSONException e) {
            throw badRequestError(e);
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "session/localization/delete", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteLocalization(@RequestBody JSONObject json) throws HttpException {
        try {
            ResponseEntity response = null;
            Long sessionId = json.getLong("sessionId");
            SessionEntity session = sessionEntityDao.findById(sessionId);
            if (session != null) {
                String localizationKey = json.getString("localizationKey");
                if (sessionEntityDao.deleteLocalization(session, localizationKey) != 0) {
                    response = ResponseEntity.ok().build();
                }
            } else {
                response = sessionNotFound(sessionId);
            }
            return response != null ? response : ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
        } catch (JSONException e) {
            throw badRequestError(e);
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "i18n/xml", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity importLocalizations(@RequestParam("file") MultipartFile file) throws HttpException {
        try {
            SessionEntity session = XmlProcessor.readXml(file.getInputStream());
            session.setAuthor(getUser());
            sessionEntityDao.createCascade(session);
            return ResponseEntity.accepted().build();
        } catch (XmlParseException e) {
            throw badRequestError(e);
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "session/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteLocalizationImportSession(@PathVariable("id") Long id)
            throws HttpException {
        try {
            ResponseEntity response;
            SessionEntity session = sessionEntityDao.findById(id);
            if (session != null) {
                sessionEntityDao.cascadeDelete(session);
                response = ResponseEntity.ok().build();
            } else {
                response = sessionNotFound(id);
            }
            return response;
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "session/apply", method = RequestMethod.POST)
    @RolesAllowed("editor")
    public ResponseEntity applySession(@RequestBody String idString) throws HttpException {
        try {
            Long id;
            try {
                id = Long.parseLong(idString);
            } catch (NumberFormatException e) {
                throw new BadRequestHttpException("invalid session id", MediaType.TEXT_PLAIN);
            }
            ResponseEntity response = null;
            SessionEntity session = sessionEntityDao.findById(id);
            if (session != null) {
                if (sessionEntityDao.applySession(session)) {
                    response = ResponseEntity.ok().build();
                }
            } else {
                response = sessionNotFound(id);
            }
            return response != null ? response : ResponseEntity
                    .status(HttpStatus.NOT_MODIFIED).build();
        } catch (SQLException e) {
            throw savingError(e);
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    private JSONObject buildSessionJson(SessionEntity session) throws JsonException {
        JSONObject json = session.toJSON();
        json.put(SessionEntity.AUTHOR_JSON, session.getAuthor().getUsername());
        return json;
    }

    private ResponseEntity sessionNotFound(Long id) {
        return ResponseEntity.status(HttpStatus.NOT_MODIFIED)
                .body("session with id: '" + id + "' does not exist");
    }

}
