package mobi.efarmer.i18n.rest.security;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author Maxim Maximchuk
 *         date 03.02.15.
 */
public enum RoleEnum implements GrantedAuthority {
    ROOT, EDITOR, VCS_MASTER;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }

    @Override
    public String getAuthority() {
        return "ROLE_" + this.name();
    }

}
