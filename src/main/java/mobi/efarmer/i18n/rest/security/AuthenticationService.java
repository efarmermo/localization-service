package mobi.efarmer.i18n.rest.security;


import com.maximchuk.oauth.resource.OAuthResourceAuthenticationService;
import com.maximchuk.oauth.resource.OAuthUser;
import mobi.efarmer.i18n.datamodel.dao.UserEntityDao;
import mobi.efarmer.i18n.datamodel.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.Date;

/**
 * @author Maxim Maximchuk
 *         date 23.12.14.
 */
@Component
public class AuthenticationService implements OAuthResourceAuthenticationService {

    @Autowired
    private UserEntityDao userEntityDao;

    @Override
    public OAuthUser processUser(String username) throws AuthenticationException {
        UserEntity user;
        try {
            user = userEntityDao.findUser(username);
            if (user == null) {
                user = userEntityDao.create(username);
            }
            user.setLastRequestDate(new Date());
            userEntityDao.update(user);
        } catch (SQLException e) {
            throw new AuthenticationServiceException(e.getMessage());
        }
        return user;
    }

    @Override
    public String processClientId(String clientId) throws AuthenticationException {
        return clientId;
    }

}
