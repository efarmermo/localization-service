package mobi.efarmer.i18n.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

/**
 * @author Maxim Maximchuk
 *         date 06.02.2016.
 */
public class BadRequestHttpException extends HttpException {
    private static final long serialVersionUID = -3951452436062506301L;

    public BadRequestHttpException(Throwable cause, MediaType responseMediaType) {
        super(cause, responseMediaType);
    }

    public BadRequestHttpException(String message, MediaType responseMediaType) {
        super(message, responseMediaType);
    }

    public BadRequestHttpException(String message, Throwable cause, MediaType responseMediaType) {
        super(message, cause, responseMediaType);
    }

    @Override
    public HttpStatus getStatusCode() {
        return HttpStatus.BAD_REQUEST;
    }
}
