package mobi.efarmer.i18n.rest.exception.resolver;

import mobi.efarmer.i18n.rest.exception.HttpException;
import mobi.efarmer.i18n.rest.exception.InternalServerErrorHttpException;
import mobi.efarmer.splunk.mint.Mint;
import mobi.efarmer.splunk.mint.MintSender;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Maxim Maximchuk
 *         date 06.02.2016.
 */
public class MintRestHttpHandlerExceptionResolver extends RestHttpHandlerExceptionResolver {

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        MintSender mintSender = Mint.prepareRequest();
        if (ex instanceof HttpException) {
            HttpException e = (HttpException) ex;

            if (e.getMintExtraData() != null) {
                mintSender.addExtraDataMap(e.getMintExtraData());
            }
        }
        mintSender.send(ex.getCause() != null ? ex.getCause() : ex,
                (ex instanceof HttpException && !(ex instanceof InternalServerErrorHttpException))
                        || ex instanceof HttpMessageNotReadableException);
        return super.resolveException(request, response, handler, ex);
    }

}
