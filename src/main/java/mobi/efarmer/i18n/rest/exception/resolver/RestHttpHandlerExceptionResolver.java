package mobi.efarmer.i18n.rest.exception.resolver;

import mobi.efarmer.i18n.rest.exception.HttpException;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;

/**
 * @author Maxim Maximchuk
 *         date 06.02.2016.
 */
public class RestHttpHandlerExceptionResolver extends DefaultHandlerExceptionResolver {
    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        if (ex instanceof HttpException) {
            HttpException e = (HttpException) ex;

            String msg;
            response.setStatus(e.getStatusCode().value());
            if (e.getResponseMediaType().equals(MediaType.APPLICATION_JSON)
                    || e.getResponseMediaType().equals(MediaType.APPLICATION_JSON_UTF8)) {
                JSONObject json = new JSONObject();
                json.put("error", e.getMessage());
                msg = json.toString();
            } else {
                msg = e.getMessage();
            }
            sendError(response, msg);
            return new ModelAndView();
        } else if (ex instanceof HttpMessageNotReadableException) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            sendError(response, ex.getMessage());
            return new ModelAndView();
        } else {
            return super.resolveException(request, response, handler, ex);
        }
    }

    private void sendError(HttpServletResponse response, String msg) {
        try (OutputStream os = response.getOutputStream()) {
            os.write(msg.getBytes("UTF-8"));
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
}
