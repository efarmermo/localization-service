package mobi.efarmer.i18n.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Maxim Maximchuk
 *         date 06.02.2016.
 */
public abstract class HttpException extends Exception {
    private static final long serialVersionUID = 5355843092100236957L;

    private MediaType responseMediaType;

    private Map<String, String> mintExtraData;

    public HttpException(Throwable cause, MediaType responseMediaType) {
        super(cause);
        this.responseMediaType = responseMediaType;
    }

    public HttpException(String message, MediaType responseMediaType) {
        super(message);
        this.responseMediaType = responseMediaType;
    }

    public HttpException(String message, Throwable cause, MediaType responseMediaType) {
        super(message, cause);
        this.responseMediaType = responseMediaType;
    }

    public MediaType getResponseMediaType() {
        return responseMediaType;
    }

    public void addMintExtraData(String key, String value) {
        if (mintExtraData == null) {
            mintExtraData = new HashMap<>();
        }
        mintExtraData.put(key, value);
    }

    public Map<String, String> getMintExtraData() {
        return mintExtraData;
    }

    public abstract HttpStatus getStatusCode();
}
