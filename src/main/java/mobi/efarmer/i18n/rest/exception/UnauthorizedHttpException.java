package mobi.efarmer.i18n.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

/**
 * @author Maxim Maximchuk
 *         date 06.02.2016.
 */
public class UnauthorizedHttpException extends HttpException {
    private static final long serialVersionUID = -3126850848594480964L;

    public UnauthorizedHttpException(Throwable cause, MediaType responseMediaType) {
        super(cause, responseMediaType);
    }

    public UnauthorizedHttpException(String message, MediaType responseMediaType) {
        super(message, responseMediaType);
    }

    public UnauthorizedHttpException(String message, Throwable cause, MediaType responseMediaType) {
        super(message, cause, responseMediaType);
    }

    @Override
    public HttpStatus getStatusCode() {
        return HttpStatus.UNAUTHORIZED;
    }
}
