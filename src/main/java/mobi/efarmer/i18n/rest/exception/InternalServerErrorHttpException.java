package mobi.efarmer.i18n.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

/**
 * @author Maxim Maximchuk
 *         date 06.02.2016.
 */
public class InternalServerErrorHttpException extends HttpException {

    private static final long serialVersionUID = -3949327650163241919L;

    public InternalServerErrorHttpException(Throwable cause, MediaType responseMediaType) {
        super(cause, responseMediaType);
    }

    public InternalServerErrorHttpException(String message, MediaType responseMediaType) {
        super(message, responseMediaType);
    }

    public InternalServerErrorHttpException(String message, Throwable cause, MediaType responseMediaType) {
        super(message, cause, responseMediaType);
    }

    @Override
    public HttpStatus getStatusCode() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }
}
