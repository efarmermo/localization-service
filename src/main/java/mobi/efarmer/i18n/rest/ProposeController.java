package mobi.efarmer.i18n.rest;

import mobi.efarmer.i18n.datamodel.dao.LocalizationEntityDao;
import mobi.efarmer.i18n.datamodel.dao.ProposalEntityDao;
import mobi.efarmer.i18n.datamodel.dao.UserEntityDao;
import mobi.efarmer.i18n.datamodel.entity.LocalizationEntity;
import mobi.efarmer.i18n.datamodel.entity.LocalizationEntryDto;
import mobi.efarmer.i18n.datamodel.entity.ProposalEntity;
import mobi.efarmer.i18n.rest.exception.HttpException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 10.02.15.
 */
@RestController
@RequestMapping("propose")
public class ProposeController extends BaseController {

    @Autowired
    private LocalizationEntityDao localizationEntityDao;

    @Autowired
    private ProposalEntityDao proposalEntityDao;

    @Autowired
    private UserEntityDao userEntityDao;

    @RequestMapping(value = "proposal", produces = MediaType.APPLICATION_JSON_VALUE)
    public JSONArray getResourcesToPropose(@RequestParam("tag") String tag,
                                           @RequestParam(value = "ver", required = false) String ver)
            throws HttpException {
        try {
            List<LocalizationEntity> localizations = ver != null ?
                    localizationEntityDao.findAllActualUsed(tag, ver + "%") :
                    localizationEntityDao.findAllActualByTag(tag);
            List<ProposalEntity> proposals = proposalEntityDao.findProposals(getUser(), false);

            JSONArray jsonArray = new JSONArray();
            for (LocalizationEntity localization : localizations) {
                ProposalEntity proposalEntity = null;
                for (ProposalEntity proposal : proposals) {
                    if (localization.getKey().equals(proposal.getKey())) {
                        proposalEntity = proposal;
                    }
                }
                if (proposalEntity != null) {
                    localizationEntityDao.refresh(proposalEntity.getLocalization());
                    JSONObject json = proposalEntity.getLocalization().toJSON();
                    json.put(ProposalEntity.PROCESSED_FIELD, proposalEntity.getProcessed());
                    jsonArray.put(json);
                } else {
                    jsonArray.put(localization.toJSON());
                }
            }
            return jsonArray;
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "proposal", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addProposals(@RequestBody JSONArray jsonArray) throws HttpException {
        try {
            try {
                List<ProposalEntity> proposalEntities = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    proposalEntities.add(new ProposalEntity(
                            new LocalizationEntryDto(jsonArray.getJSONObject(i)),
                            getUser()));
                }

                ResponseEntity response;
                if (proposalEntityDao.createBatch(proposalEntities) > 0) {
                    response = ResponseEntity.accepted().build();
                } else {
                    response = ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                }
                return response;
            } catch (SQLException e) {
                throw savingError(e);
            }
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "unprocessed", produces = MediaType.APPLICATION_JSON_VALUE)
    public JSONArray getAllProposal() throws HttpException {
        try {
            List<ProposalEntity> entities = proposalEntityDao.findUnprocessed();
            JSONArray jsonArray = new JSONArray();
            for (ProposalEntity entity : entities) {
                localizationEntityDao.refresh(entity.getLocalization());

                JSONObject obj = entity.toJSON();
                userEntityDao.refresh(entity.getAuthor());
                obj.put(ProposalEntity.AUTHOR_JSON_FIELD, entity.getAuthor().getUsername());
                jsonArray.put(obj);
            }
            return jsonArray;
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "proposal/{id}", method = RequestMethod.DELETE)
    public ResponseEntity rejectProposal(@PathVariable("id") Long id) throws HttpException {
        try {
            ResponseEntity response;
            if (proposalEntityDao.processed(id)) {
                response = ResponseEntity.ok().build();
            } else {
                response = ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            return response;
        } catch (Exception e) {
            throw internalError(e);
        }
    }

}
