package mobi.efarmer.i18n.rest;

import mobi.efarmer.i18n.datamodel.dao.HeadEntityDao;
import mobi.efarmer.i18n.datamodel.dao.HistoryEntityDao;
import mobi.efarmer.i18n.datamodel.dao.LocalizationEntityDao;
import mobi.efarmer.i18n.datamodel.entity.HeadEntity;
import mobi.efarmer.i18n.datamodel.entity.HistoryEntity;
import mobi.efarmer.i18n.datamodel.entity.LocalizationEntity;
import mobi.efarmer.i18n.rest.exception.HttpException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 09.12.2014.
 */
@RestController
@RequestMapping("vc")
public class VersionControlController extends BaseController {

    @Autowired
    private HeadEntityDao headEntityDao;

    @Autowired
    private HistoryEntityDao historyEntityDao;

    @Autowired
    private LocalizationEntityDao localizationEntityDao;

    @RequestMapping(value = "head/{key}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getHead(@PathVariable("key") String key) throws HttpException {
        try {
            ResponseEntity response;
            if (key != null) {
                HeadEntity head = headEntityDao.findByKey(key);
                if (head != null) {
                    localizationEntityDao.refresh(head.getLocalization());
                }
                response = ResponseEntity.ok(head != null ? head : new JSONObject());
            } else {
                response = ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            return response;
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "head", method = RequestMethod.POST)
    public ResponseEntity setHead(@RequestParam("id") Long id) throws HttpException {
        try {
            ResponseEntity response;
            if (id != null && headEntityDao.setHead(id)) {
                response = ResponseEntity.accepted().build();
            } else {
                response = ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            return response;
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "history/header/{key}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<HistoryEntity> historyHeader(@PathVariable("key") String key) throws HttpException {
        try {
            return historyEntityDao.getHistory(key);
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "history/{key}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<HistoryEntity> history(@PathVariable("key") String key) throws HttpException {
        try {
            List<HistoryEntity> history = historyEntityDao.getHistory(key);
            for (HistoryEntity entity : history) {
                localizationEntityDao.refresh(entity.getLocalization());
                entity.getLocalization().addFieldToJsonIgnore(LocalizationEntity.KEY_FIELD);
                entity.addFieldToJsonIgnore(HistoryEntity.KEY_FIELD);
            }
            return history;
        } catch (Exception e) {
            throw internalError(e);
        }
    }

}
