package mobi.efarmer.i18n.rest;

import mobi.efarmer.i18n.datamodel.dao.HeadEntityDao;
import mobi.efarmer.i18n.datamodel.dao.LocalizationEntityDao;
import mobi.efarmer.i18n.datamodel.dao.TranslateRequestEntityDao;
import mobi.efarmer.i18n.datamodel.entity.HeadEntity;
import mobi.efarmer.i18n.datamodel.entity.LocalizationEntity;
import mobi.efarmer.i18n.datamodel.entity.LocalizationEntryDto;
import mobi.efarmer.i18n.datamodel.entity.TranslateRequestEntity;
import mobi.efarmer.i18n.rest.exception.BadRequestHttpException;
import mobi.efarmer.i18n.rest.exception.HttpException;
import mobi.efarmer.i18n.rest.exception.InternalServerErrorHttpException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.Base64;
import java.util.Date;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 09.12.2014.
 */
@RestController
@RequestMapping("i18n")
public class PublicController extends BaseController {

    @Autowired
    private LocalizationEntityDao localizationEntityDao;

    @Autowired
    private HeadEntityDao headEntityDao;

    @Autowired
    private TranslateRequestEntityDao translateRequestEntityDao;

    @RequestMapping(value = "list", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<LocalizationEntity> getResources(@RequestParam(value = "tag", required = false) String tag)
            throws HttpException {
        try {
            List<LocalizationEntity> localizations;
            if (tag != null) {
                localizations = localizationEntityDao.findAllActualByTag(tag);
            } else {
                localizations = localizationEntityDao.findAllActual();
            }
            return localizations;
        } catch (Exception e) {
            throw new InternalServerErrorHttpException(e, MediaType.APPLICATION_JSON);
        }
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getResource(@RequestParam("key") String base64Key) throws HttpException {
        try {
            String key = new String(Base64.getDecoder().decode(base64Key));
            HeadEntity head = headEntityDao.findByKey(key);
            ResponseEntity response;
            if (head != null) {
                response = ResponseEntity.ok(new LocalizationEntryDto(head, localizationEntityDao));
            } else {
                response = ResponseEntity.noContent().build();
            }
            return response;
        } catch (SQLException e) {
            throw new InternalServerErrorHttpException(e, MediaType.APPLICATION_JSON);
        }
    }

    @RequestMapping(value = "translate/{key}", produces = MediaType.APPLICATION_JSON_VALUE)
    public LocalizationEntity translate(@PathVariable("key") String key) throws HttpException {
        try {
            return localizationEntityDao.findActual(key);
        } catch (Exception e) {
            throw new InternalServerErrorHttpException(e, MediaType.APPLICATION_JSON);
        }
    }

    @RequestMapping(value = "translate/{lang}/{key}", produces = MediaType.TEXT_PLAIN_VALUE)
    public String translate(@PathVariable("lang") String lang, @PathVariable("key") String key)
            throws HttpException {
        try {
            LocalizationEntity localization = localizationEntityDao.findActual(key);
            String translate = "";
            if (localization != null) {
                translate = localization.getI18n().getString(lang);
            }
            return translate;
        } catch (Exception e) {
            throw new InternalServerErrorHttpException(e, MediaType.TEXT_PLAIN);
        }
    }

    @RequestMapping(value = "updates/{time}", produces = MediaType.APPLICATION_JSON_VALUE)
    public JSONObject getUpdates(@PathVariable("time") Long time,
                                 @RequestParam(value = "tag", required = false) String tag)
            throws HttpException {
        try {
            Date date = new Date(time);
            List<LocalizationEntity> list = localizationEntityDao.findNewest(date, tag);
            JSONObject json = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            for (LocalizationEntity entity : list) {
                entity.addFieldToJsonIgnore(LocalizationEntity.ID_FIELD);
                jsonArray.put(entity.toJSON());
            }
            json.put("date", new Date().getTime());
            json.put("data", jsonArray);
            return json;
        } catch (Exception e) {
            throw new InternalServerErrorHttpException(e, MediaType.APPLICATION_JSON);
        }
    }

    @RequestMapping(value = "tags", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getTags(@RequestParam(value = "key", required = false) String key) throws HttpException {
        try {
            ResponseEntity response;
            if (key == null) {
                response = ResponseEntity.ok(headEntityDao.findAllTags());
            } else {
                HeadEntity head = headEntityDao.findByKey(key);
                if (head != null) {
                    JSONArray tags = new JSONArray();
                    if (head.getTags() != null) {
                        for (String tag : head.getTags().split(",")) {
                            tags.put(tag);
                        }
                    }
                    response = ResponseEntity.ok(tags);
                } else {
                    response = ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                }
            }
            return response;
        } catch (Exception e) {
            throw new InternalServerErrorHttpException(e, MediaType.APPLICATION_JSON);
        }
    }

    @RequestMapping(value = "request", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity translateRequest(@RequestBody TranslateRequestEntity translateRequest) throws HttpException {
        try {
            return translateRequestEntityDao.create(translateRequest) > 0 ?
                    ResponseEntity.accepted().build() :
                    ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
        } catch (SQLException e) {
            HttpException ex = savingError(e);
            ex.addMintExtraData("method", "translate request");
            if (translateRequest.getApplicationName() != null) {
                ex.addMintExtraData("application_name", translateRequest.getApplicationName());
            }
            if (translateRequest.getApplicationVersion() != null) {
                ex.addMintExtraData("application_version", translateRequest.getApplicationVersion());
            }
            if (translateRequest.getKey() != null) {
                ex.addMintExtraData("requested_key", translateRequest.getKey());
            }
            throw ex;
        } catch (Exception e) {
            throw new InternalServerErrorHttpException(e, MediaType.APPLICATION_JSON_UTF8);
        }
    }

}
