package mobi.efarmer.i18n.rest;

import mobi.efarmer.i18n.datamodel.dao.HeadEntityDao;
import mobi.efarmer.i18n.datamodel.dao.HistoryEntityDao;
import mobi.efarmer.i18n.datamodel.dao.ProposalEntityDao;
import mobi.efarmer.i18n.datamodel.dao.TranslateRequestEntityDao;
import mobi.efarmer.i18n.datamodel.entity.HistoryEntity;
import mobi.efarmer.i18n.datamodel.entity.LocalizationEntryDto;
import mobi.efarmer.i18n.datamodel.entity.TranslateRequestEntity;
import mobi.efarmer.i18n.rest.exception.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 10.12.2014.
 */
@RestController
@RequestMapping("edit")
public class EditController extends BaseController {

    @Context
    private HttpServletRequest request;

    @Autowired
    private HistoryEntityDao historyEntityDao;

    @Autowired
    private TranslateRequestEntityDao translateRequestEntityDao;

    @Autowired
    private ProposalEntityDao proposalEntityDao;

    @Autowired
    private HeadEntityDao headEntityDao;

    @RequestMapping(value = "i18n", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addLocalization(@RequestBody LocalizationEntryDto entry) throws HttpException {
        try {
            try {
                HistoryEntity entity = new HistoryEntity(entry, getUser(), proposalEntityDao, translateRequestEntityDao);
                ResponseEntity response;
                if (historyEntityDao.create(entity) > 0) {
                    response = ResponseEntity.accepted().build();
                } else {
                    response = ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                }
                return response;
            } catch (SQLException e) {
                throw badRequestError(e);
            }
        } catch (Exception e) {
            throw  internalError(e);
        }
    }

    @RequestMapping(value = "requests")
    public List<TranslateRequestEntity> getRequests() throws HttpException {
        try {
            return translateRequestEntityDao.allUnprocessed();
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "i18n/{key}", method = RequestMethod.DELETE)
    public ResponseEntity deleteLocalization(@PathVariable("key") String key) throws HttpException {
        try {
            if (headEntityDao.deleteByLocalizationKey(key)) {
                return ResponseEntity.ok().build();
            }
            return ResponseEntity.status(HttpStatus.GONE).build();
        } catch (Exception e) {
            throw internalError(e);
        }
    }

    @RequestMapping(value = "i18n/request/{id}", method = RequestMethod.DELETE)
    public ResponseEntity rejectTranslateRequest(@PathVariable("id") Long id) throws HttpException {
        try {
            ResponseEntity response;
            if (translateRequestEntityDao.reject(id)) {
                response = ResponseEntity.ok().build();
            } else {
                response = ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            return response;
        } catch (Exception e) {
            throw internalError(e);
        }
    }

}
