package mobi.efarmer.i18n.rest;

import com.maximchuk.json.exception.JsonException;
import com.maximchuk.oauth.resource.OAuthPrincipal;
import mobi.efarmer.i18n.datamodel.entity.UserEntity;
import mobi.efarmer.i18n.rest.exception.BadRequestHttpException;
import mobi.efarmer.i18n.rest.exception.HttpException;
import mobi.efarmer.i18n.rest.exception.InternalServerErrorHttpException;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 09.12.2014.
 */
public class BaseController {

    protected Logger log = Logger.getLogger(getClass());

    protected HttpException savingError(SQLException e) {
        log.error(e.getMessage());
        return new BadRequestHttpException("saving error", e, MediaType.APPLICATION_JSON);
    }

    protected HttpException internalError(Exception e) {
        log.error(e.getMessage());
        return new InternalServerErrorHttpException(e, MediaType.APPLICATION_JSON);
    }

    protected HttpException invalidJsonError(JsonException e) {
        log.info(e.getMessage());
        return new BadRequestHttpException("Invalid json request: " + e.getMessage(), e, MediaType.APPLICATION_JSON);
    }

    protected HttpException badRequestError(Exception e) {
        log.info(e.getMessage());
        return new BadRequestHttpException(e.getMessage(), e, MediaType.APPLICATION_JSON);
    }

    protected UserEntity getUser() {
        return getPrincipal().getUser();
    }

    private OAuthPrincipal getPrincipal() {
        return (OAuthPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
