package mobi.efarmer.i18n.config;

import com.j256.ormlite.db.PostgresDatabaseType;
import com.j256.ormlite.jdbc.DataSourceConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * created by Raymond Keller on 14.03.2016.
 */
@Configuration
@PropertySource("classpath:app.properties")
@ComponentScan({"mobi.efarmer.i18n.rest", "mobi.efarmer.i18n.datamodel.dao"})
@Import({SecurityConfig.class})
public class SpringRootConfig {

    private static final String PROP_DS_JNDI_NAME = "datasource.jndiName";

    @Resource
    private Environment env;

    @Bean
    public ConnectionSource connectionSource() {
        ConnectionSource connectionSource = null;

        try {
            DataSource ds = (DataSource) new InitialContext().lookup(env.getRequiredProperty(PROP_DS_JNDI_NAME));
            connectionSource = new DataSourceConnectionSource(ds, new PostgresDatabaseType());
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
        }

        return connectionSource;
    }
}
