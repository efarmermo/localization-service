package mobi.efarmer.i18n.config;

import com.maximchuk.oauth.resource.OAuthAuthenticationEntryPoint;
import com.maximchuk.oauth.resource.OAuthResourceAuthenticationProvider;
import com.maximchuk.oauth.resource.OAuthTokenPreAuthenticationFilter;
import mobi.efarmer.i18n.rest.security.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

/**
 * created by Raymond Keller on 15.03.2016.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private static final String PROP_OAUTH_SERVER_URL = "oauthserver.url";

    @Autowired
    private AuthenticationService authService;

    @Autowired
    private Environment env;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf()
                .disable()
            .addFilterAfter(preAuthFilter(), AbstractPreAuthenticatedProcessingFilter.class)
            .authenticationProvider(authenticationProvider())
            .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint())
                .and()
            .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
            .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS)
                .permitAll()
                .and()
            .authorizeRequests()
                .antMatchers("/api/user/**")
                .authenticated()
                .and()
            .authorizeRequests()
                .antMatchers("/api/edit/**")
                .hasRole("EDITOR")
                .and()
            .authorizeRequests()
                .antMatchers("/api/import/**")
                .hasRole("EDITOR")
                .and()
            .authorizeRequests()
                .antMatchers("/api/propose/unprocessed")
                .hasRole("EDITOR")
                .and()
            .authorizeRequests()
                .antMatchers("/api/propose/proposal/**")
                .hasRole("EDITOR")
                .and()
            .authorizeRequests()
                .antMatchers("/api/vc/**")
                .hasRole("VCS_MASTER");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .authenticationProvider(authenticationProvider());
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        return new OAuthResourceAuthenticationProvider(env.getRequiredProperty(PROP_OAUTH_SERVER_URL), authService);
    }

    @Bean
    public OAuthTokenPreAuthenticationFilter preAuthFilter() throws Exception {
        OAuthTokenPreAuthenticationFilter oAuthTokenPreAuthenticationFilter = new OAuthTokenPreAuthenticationFilter();
        oAuthTokenPreAuthenticationFilter.setAuthenticationManager(authenticationManager());

        return oAuthTokenPreAuthenticationFilter;
    }

    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
        return new OAuthAuthenticationEntryPoint();
    }
}
