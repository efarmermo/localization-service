package mobi.efarmer.i18n.config;

        import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * created by Raymond Keller on 16.03.2016.
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
