package mobi.efarmer.i18n.config;

import mobi.efarmer.i18n.rest.converter.*;
import mobi.efarmer.i18n.rest.exception.resolver.MintRestHttpHandlerExceptionResolver;
import mobi.efarmer.splunk.mint.AppInfo;
import mobi.efarmer.splunk.mint.Credential;
import mobi.efarmer.splunk.mint.Mint;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.Resource;
import java.util.List;

/**
 * created by Raymond Keller on 14.03.2016.
 */
@ComponentScan({"mobi.efarmer.i18n.rest"})
@EnableWebMvc
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    private static final String APP_VERSION = "app.version";
    private static final String MINT_API_KEY = "mint.api.key";
    private static final String MINT_TOKEN = "mint.token";

    @Resource
    private Environment env;

    @Bean
    public InitializingBean initializingBean() {
        return () ->
                Mint.init(new Credential(env.getProperty(MINT_API_KEY), env.getProperty(MINT_TOKEN)),
                new AppInfo("mobi.efarmer.i18n", "localization-center", env.getProperty(APP_VERSION)));
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        resolver.setDefaultEncoding("utf-8");
        return resolver;
    }
    @Bean
    MappingJackson2HttpMessageConverter jacksonConverter() {
        return new MappingJackson2HttpMessageConverter();
    }
    @Override
    public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
//        exceptionResolvers.add(new RestHttpHandlerExceptionResolver());
        exceptionResolvers.add(new MintRestHttpHandlerExceptionResolver());
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(new JSONObjectHttpMessageConverter());
        converters.add(new JSONArrayHttpMessageConverter());
        converters.add(new JsonDTOHttpMessageConverter());
        converters.add(new JsonDTOListHttpMessageConverter());
        converters.add(new String2XmlHttpMessageConverter());
        converters.add(jacksonConverter());

    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedHeaders("*")
                .allowedMethods("*")
                .allowCredentials(true);
    }

}
