package mobi.efarmer.i18n.datamodel.dao;

import com.j256.ormlite.support.ConnectionSource;
import mobi.efarmer.i18n.datamodel.entity.IEntity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 19.02.15.
 */
public abstract class BaseManyToManyEntityDaoImpl<T extends IEntity, ID, A extends IEntity, B extends IEntity> extends BaseEntityDao<T, ID>
        implements ManyToManyEntityDao<T, ID, A, B> {

    protected BaseManyToManyEntityDaoImpl(ConnectionSource connectionSource, Class<T> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public T createIfNotExists(A entity1, B entity2) throws SQLException {
        return createIfNotExists(connect(entity1, entity2));
    }

    public List<T> batchCreate(List<A> list1, List<B> list2) throws SQLException {
        return callBatchTasks(() -> {
            List<T> result = new ArrayList<T>();
            for (A entity1: list1) {
                for (B entity2 : list2) {
                    T created = createIfNotExists(entity1, entity2);
                    if (created != null) {
                        result.add(created);
                    }
                }
            }
            return result;
        });
    }

}
