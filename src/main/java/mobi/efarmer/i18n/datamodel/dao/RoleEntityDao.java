package mobi.efarmer.i18n.datamodel.dao;

import com.j256.ormlite.support.ConnectionSource;
import mobi.efarmer.i18n.datamodel.entity.RoleEntity;
import mobi.efarmer.i18n.rest.security.RoleEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 03.02.15.
 */
@Repository
public class RoleEntityDao extends BaseEntityDao<RoleEntity, Long> {

    @Autowired
    public RoleEntityDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, RoleEntity.class);
    }

    public RoleEntity getRoleEntity(RoleEnum role) throws SQLException {
        RoleEntity roleEntity = queryBuilder().where().eq(RoleEntity.ROLE_ENUM_FIELD, role).queryForFirst();
        if (roleEntity == null) {
            roleEntity = createIfNotExists(new RoleEntity(role));
        }
        return roleEntity;
    }

}
