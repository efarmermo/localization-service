package mobi.efarmer.i18n.datamodel.dao;

import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.support.ConnectionSource;
import mobi.efarmer.i18n.datamodel.entity.RoleEntity;
import mobi.efarmer.i18n.datamodel.entity.UserEntity;
import mobi.efarmer.i18n.rest.security.RoleEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 23.12.14.
 */
@Repository
public class UserEntityDao extends BaseEntityDao<UserEntity, Long> {

    @Autowired
    private RoleEntityDao roleEntityDao;

    @Autowired
    private UserRoleEntityDao userRoleEntityDao;

    @Autowired
    public UserEntityDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, UserEntity.class);
    }

    public UserEntity findUser(String username) throws SQLException {
        return queryBuilder().where().eq(UserEntity.USERNAME_FIELD, username).queryForFirst();
    }

    public UserEntity create(String username) throws SQLException {
        UserEntity user = new UserEntity();
        user.setUsername(username);
        UserEntity createdUser = createIfNotExists(user);
        return initRootUserIfNeeded(createdUser);
    }

    public UserEntity addRole(UserEntity user, RoleEnum role) throws SQLException {
        return TransactionManager
                .callInTransaction(getConnectionSource(), () -> {
                    RoleEntity roleEntity = roleEntityDao.getRoleEntity(role);
                    return userRoleEntityDao.createIfNotExists(user, roleEntity).getUser();
                });
    }

    private UserEntity initRootUserIfNeeded(UserEntity user) throws SQLException {
        if (queryBuilder().countOf() == 1) {
            for (RoleEnum role: RoleEnum.values()) {
                addRole(user, role);
            }
            refresh(user);
        }
        return user;
    }

}
