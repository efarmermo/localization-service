package mobi.efarmer.i18n.datamodel.dao.dataimport;

import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.support.ConnectionSource;
import mobi.efarmer.i18n.datamodel.dao.BaseEntityDao;
import mobi.efarmer.i18n.datamodel.dao.HeadEntityDao;
import mobi.efarmer.i18n.datamodel.dao.HistoryEntityDao;
import mobi.efarmer.i18n.datamodel.dao.LocalizationEntityDao;
import mobi.efarmer.i18n.datamodel.entity.HeadEntity;
import mobi.efarmer.i18n.datamodel.entity.HistoryEntity;
import mobi.efarmer.i18n.datamodel.entity.LocalizationEntity;
import mobi.efarmer.i18n.datamodel.entity.dataimport.ResourceEntity;
import mobi.efarmer.i18n.datamodel.entity.dataimport.SessionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 18.02.15.
 */
@Repository
public class SessionEntityDao extends BaseEntityDao<SessionEntity, Long> {

    @Autowired
    private LocalizationEntityDao localizationEntityDao;

    @Autowired
    private HeadEntityDao headEntityDao;

    @Autowired
    private HistoryEntityDao historyEntityDao;

    @Autowired
    private ResourceEntityDao resourceEntityDao;

    @Autowired
    public SessionEntityDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, SessionEntity.class);
    }

    public SessionEntity createCascade(SessionEntity sessionEntity) throws SQLException {
        return TransactionManager.callInTransaction(getConnectionSource(), () -> {
            SessionEntity created = createIfNotExists(sessionEntity);
            resourceEntityDao.batchCreate(Arrays.asList(sessionEntity),
                    localizationEntityDao.batchCreate(sessionEntity.getLocalizations()));
            return created;
        });
    }

    public int cascadeDelete(SessionEntity sessionEntity) throws SQLException {
        return TransactionManager.callInTransaction(getConnectionSource(), () -> {
            List<ResourceEntity> resourceEntityList = resourceEntityDao.findBySession(sessionEntity);
            for (ResourceEntity resourceEntity : resourceEntityList) {
                resourceEntityDao.delete(resourceEntity);
                localizationEntityDao.delete(resourceEntity.getLocalization());
            }
            return delete(sessionEntity);
        });
    }

    public List<SessionEntity> getAll() throws SQLException {
        return queryForAll();
    }

    public int deleteLocalization(SessionEntity session, String localizationKey) throws SQLException {
        int res = 0;
        ResourceEntity resourceEntity = resourceEntityDao.findBySessionAndLocalizationKey(session, localizationKey);
        if (resourceEntity != null) {
            res = TransactionManager.callInTransaction(getConnectionSource(), () -> {
                LocalizationEntity localizationEntity = resourceEntity.getLocalization();
                resourceEntityDao.delete(resourceEntity);
                return localizationEntityDao.delete(localizationEntity);
            });
        }
        return res;
    }

    public int updateLocalization(SessionEntity session, LocalizationEntity localizationEntity) throws SQLException {
        int res = 0;
        ResourceEntity resourceEntity = resourceEntityDao.findBySessionAndLocalizationKey(session, localizationEntity.getKey());
        if (resourceEntity != null) {
            localizationEntity.setId(resourceEntity.getLocalization().getId());
            res = localizationEntityDao.update(localizationEntity);
        }
        return res;
    }

    public boolean applySession(SessionEntity sessionEntity) throws SQLException {
        final List<HistoryEntity> historyEntities = new ArrayList<>();
        for (LocalizationEntity localizationEntity : localizationEntityDao.findByImportSession(sessionEntity)) {
            List<String> tags = new ArrayList<>(Arrays.asList(sessionEntity.getSrcName().split(",")));
            HeadEntity headEntity = headEntityDao.findByKey(localizationEntity.getKey());
            if (headEntity != null) {
                for (String hTag : headEntity.getTags().split(",")) {
                    boolean contain = false;
                    for (String tag : tags) {
                        if (hTag.equals(tag)) {
                            contain = true;
                            break;
                        }
                    }
                    if (!contain) {
                        tags.add(hTag);
                    }
                }
            }
            HistoryEntity historyEntity = new HistoryEntity(localizationEntity, sessionEntity.getAuthor());
            historyEntity.setTags(tags.stream().reduce((t, u) -> t + ',' + u).get());
            historyEntities.add(historyEntity);
        }
        return TransactionManager.callInTransaction(getConnectionSource(), () -> {
            boolean res = false;
            if (historyEntityDao.batchCreate(historyEntities) > 0) {
                res = cascadeDelete(sessionEntity) > 0;
            }
            return res;
        });
    }

}
