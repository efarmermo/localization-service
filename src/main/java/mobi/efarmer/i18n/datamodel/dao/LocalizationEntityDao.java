package mobi.efarmer.i18n.datamodel.dao;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import mobi.efarmer.i18n.datamodel.dao.dataimport.ResourceEntityDao;
import mobi.efarmer.i18n.datamodel.entity.HeadEntity;
import mobi.efarmer.i18n.datamodel.entity.HeadUsageEntity;
import mobi.efarmer.i18n.datamodel.entity.LocalizationEntity;
import mobi.efarmer.i18n.datamodel.entity.dataimport.SessionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 09.12.2014.
 */
@Repository
public class LocalizationEntityDao extends BaseEntityDao<LocalizationEntity, Long> {

    @Autowired
    private HeadEntityDao headEntityDao;

    @Autowired
    private HeadUsageEntityDao headUsageEntityDao;

    @Autowired
    private ResourceEntityDao resourceEntityDao;

    @Autowired
    public LocalizationEntityDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, LocalizationEntity.class);
    }

    public List<LocalizationEntity> batchCreate(List<LocalizationEntity> localizationEntities) throws SQLException {
        return callBatchTasks(() -> {
            List<LocalizationEntity> result = new ArrayList<>();
            for (LocalizationEntity entity : localizationEntities) {
                result.add(createIfNotExists(entity));
            }
            return result;
        });
    }

    public LocalizationEntity findActual(String key) throws SQLException {
        return queryBuilder().join(headEntityDao.queryBuilderByKey(key)).queryForFirst();
    }

    public List<LocalizationEntity> findNewest(Date date) throws SQLException {
        return findNewest(date, null);
    }

    public List<LocalizationEntity> findNewest(Date date, String tag) throws SQLException {
        QueryBuilder<HeadEntity, Long> headQb = headEntityDao.queryBuilder();
        Where<HeadEntity, Long> where = headEntityDao.newestThan(headQb.where(), date);
        if (tag != null) {
            headEntityDao.tagCriteria(where, tag);
            where.and(2);
        }
        return queryBuilder().join(headQb).query();
    }

    public List<LocalizationEntity> findAllActual() throws SQLException {
        return queryBuilder().join(headEntityDao.queryBuilder()).orderBy(LocalizationEntity.KEY_FIELD, true).query();
    }

    public List<LocalizationEntity> findAllActualByTag(String tag) throws SQLException {
        return queryBuilder().join(headEntityDao.queryBuilderByTag(tag)).orderBy(LocalizationEntity.KEY_FIELD, true).query();
    }

    public List<LocalizationEntity> findAllActualUsed(String tag, String versionPattern) throws SQLException {
        QueryBuilder<HeadUsageEntity, Long> headUsageQueryBuilder = headUsageEntityDao.queryBuilder();
        headUsageQueryBuilder.selectColumns(HeadUsageEntity.KEY_FIELD)
                .where().eq(HeadUsageEntity.APPLICATION_NAME_FIELD, tag)
                .and().like(HeadUsageEntity.APPLICATION_VERSION_FIELD, versionPattern);

        QueryBuilder<HeadEntity, Long> headQueryBuilder = headEntityDao.queryBuilder();
        headEntityDao.tagCriteria(headQueryBuilder.where(), tag)
                .and().in(HeadEntity.KEY_FIELD, headUsageQueryBuilder);

        return queryBuilder().join(headQueryBuilder).query();
    }

    public List<LocalizationEntity> findByImportSession(SessionEntity sessionEntity) throws SQLException {
        return queryBuilder().join(resourceEntityDao.queryBuilderByImportSession(sessionEntity)).query();
    }

    public QueryBuilder<LocalizationEntity, Long> queryBuilderByKey(String key) throws SQLException {
        QueryBuilder<LocalizationEntity, Long> queryBuilder = queryBuilder();
        queryBuilder.where().eq(LocalizationEntity.KEY_FIELD, key);
        return queryBuilder;
    }
}
