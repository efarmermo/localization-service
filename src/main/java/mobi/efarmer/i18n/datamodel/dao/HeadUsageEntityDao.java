package mobi.efarmer.i18n.datamodel.dao;

import com.j256.ormlite.support.ConnectionSource;
import mobi.efarmer.i18n.datamodel.entity.HeadUsageEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;

/**
 * Created by Maxim Maximchuk
 * date 12-Jan-16.
 */
@Repository
public class HeadUsageEntityDao extends BaseEntityDao<HeadUsageEntity, Long> {

    @Autowired
    public HeadUsageEntityDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, HeadUsageEntity.class);
    }

    @Override
    public HeadUsageEntity createIfNotExists(final HeadUsageEntity data) throws SQLException {
        HeadUsageEntity createdEntity = null;

            HeadUsageEntity entity = queryBuilder().where()
                    .eq(HeadUsageEntity.KEY_FIELD, data.getKey())
                    .and().eq(HeadUsageEntity.APPLICATION_NAME_FIELD, data.getApplicationName())
                    .and().eq(HeadUsageEntity.APPLICATION_VERSION_FIELD, data.getApplicationVersion())
                    .queryForFirst();

            if (entity == null) {
                createdEntity = super.createIfNotExists(data);
            }
        return createdEntity != null? createdEntity: data;
    }

}
