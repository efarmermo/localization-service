package mobi.efarmer.i18n.datamodel.dao;

import com.j256.ormlite.dao.Dao;
import mobi.efarmer.i18n.datamodel.entity.IEntity;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 19.02.15.
 */
public interface ManyToManyEntityDao<T extends IEntity, ID, A extends IEntity, B extends IEntity> extends Dao<T, ID> {

    T connect(A entity1, B entity2) throws SQLException;

}
