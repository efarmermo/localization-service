package mobi.efarmer.i18n.datamodel.dao;

import com.j256.ormlite.support.ConnectionSource;
import mobi.efarmer.i18n.datamodel.entity.RoleEntity;
import mobi.efarmer.i18n.datamodel.entity.UserEntity;
import mobi.efarmer.i18n.datamodel.entity.UserRoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 03.02.15.
 */
@Repository
public class UserRoleEntityDao extends BaseManyToManyEntityDaoImpl<UserRoleEntity, Long, UserEntity, RoleEntity> {

    @Autowired
    protected UserRoleEntityDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, UserRoleEntity.class);
    }

    @Override
    public UserRoleEntity connect(UserEntity user, RoleEntity role) throws SQLException {
        UserRoleEntity userRole = new UserRoleEntity();
        userRole.setUser(user);
        userRole.setRole(role);
        return userRole;
    }
}
