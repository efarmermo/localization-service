package mobi.efarmer.i18n.datamodel.dao;

import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import mobi.efarmer.i18n.datamodel.entity.HeadEntity;
import mobi.efarmer.i18n.datamodel.entity.HistoryEntity;
import mobi.efarmer.i18n.datamodel.entity.LocalizationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.*;

/**
 * @author Maxim Maximchuk
 *         date 09.12.2014.
 */
@Repository
public class HeadEntityDao extends BaseEntityDao<HeadEntity, Long> {

    @Autowired
    private HistoryEntityDao historyEntityDao;

    @Autowired
    private LocalizationEntityDao localizationEntityDao;

    @Autowired
    private TranslateRequestEntityDao translateRequestEntityDao;

    @Autowired
    public HeadEntityDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, HeadEntity.class);
    }

    public boolean setHead(Long id) throws SQLException {
        HistoryEntity historyEntity = historyEntityDao.findById(id);
        DatabaseConnection connection = startThreadConnection();
        connection.setAutoCommit(false);
        try {
            if (historyEntity != null) {
                DeleteBuilder<HeadEntity, Long> db = deleteBuilder();
                db.where().eq(HeadEntity.KEY_FIELD, historyEntity.getKey());
                db.delete();

                create(new HeadEntity(historyEntity));
                commit(connection);
                return true;
            }
        } catch (SQLException e) {
            rollBack(connection);
            throw e;
        } finally {
            endThreadConnection(connection);
        }
        return false;
    }

    public List<HeadEntity> findAll() throws SQLException {
        return queryForAll();
    }

    public List<HeadEntity> findAllByTag(String tag) throws SQLException {
        return queryBuilderByTag(tag).query();
    }

    public HeadEntity findByKey(String key) throws SQLException {
        return queryBuilderByKey(key).queryForFirst();
    }

    public boolean deleteByLocalizationKey(String key) throws SQLException {
        return TransactionManager.callInTransaction(getConnectionSource(), () -> {
            LocalizationEntity localizationEntity = localizationEntityDao.findActual(key);
            DeleteBuilder deleteBuilder = deleteBuilder();
            deleteBuilder.where().eq(HeadEntity.LOCALIZATION_ID_FIELD, localizationEntity.getId());

            boolean deleted = deleteBuilder.delete() > 0;
            if (deleted) {
                translateRequestEntityDao.rejectByKey(key);
            }
            return deleted;
        });
    }

    public Set<String> findAllTags() throws SQLException {
        Set<String> tagSet = new HashSet<>();
        List<HeadEntity> heads = queryForAll();
        for (HeadEntity head: heads) {
            if (head.getTags() != null) {
                String[] tags = head.getTags().split(",");
                Collections.addAll(tagSet, tags);
            }
        }
        return tagSet;
    }

    public QueryBuilder<HeadEntity, Long> queryBuilderByKey(String key) throws SQLException {
        QueryBuilder<HeadEntity, Long> qb = queryBuilder();
        qb.where().eq(HeadEntity.KEY_FIELD, key);
        return qb;
    }

    public Where<HeadEntity, Long> newestThan(Where<HeadEntity, Long> where, Date date) throws SQLException {
        where.gt(HeadEntity.UPDATE_TIMESTAMP_FIELD, date);
        return where;
    }

    public Where<HeadEntity, Long> tagCriteria(Where<HeadEntity, Long> where, String tag) throws SQLException {
        return where.like(HeadEntity.TAGS_FIELD, tag + ",%")
                .or().like(HeadEntity.TAGS_FIELD, "%," + tag + ",%")
                .or().like(HeadEntity.TAGS_FIELD, "%," + tag)
                .or().like(HeadEntity.TAGS_FIELD, tag);
    }

    public QueryBuilder<HeadEntity, Long> queryBuilderByTag(String tag) throws SQLException {
        QueryBuilder<HeadEntity, Long> qb = queryBuilder();
        tagCriteria(qb.where(), tag);
        return qb;
    }

}
