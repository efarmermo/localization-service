package mobi.efarmer.i18n.datamodel.dao.dataimport;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import mobi.efarmer.i18n.datamodel.dao.BaseManyToManyEntityDaoImpl;
import mobi.efarmer.i18n.datamodel.dao.LocalizationEntityDao;
import mobi.efarmer.i18n.datamodel.entity.LocalizationEntity;
import mobi.efarmer.i18n.datamodel.entity.dataimport.ResourceEntity;
import mobi.efarmer.i18n.datamodel.entity.dataimport.SessionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 19.02.15.
 */
@Repository
public class ResourceEntityDao extends BaseManyToManyEntityDaoImpl<ResourceEntity, Long, SessionEntity, LocalizationEntity> {

    @Autowired
    private LocalizationEntityDao localizationEntityDao;

    @Autowired
    public ResourceEntityDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, ResourceEntity.class);
    }

    @Override
    public ResourceEntity connect(SessionEntity sessionEntity, LocalizationEntity localizationEntity) throws SQLException {
        ResourceEntity resource = new ResourceEntity();
        resource.setSession(sessionEntity);
        resource.setLocalization(localizationEntity);
        return resource;
    }

    public List<ResourceEntity> findBySession(SessionEntity sessionEntity) throws SQLException {
        return queryBuilderByImportSession(sessionEntity).query();
    }

    public ResourceEntity findBySessionAndLocalizationKey(SessionEntity sessionEntity, String key) throws SQLException {
        return queryBuilderByImportSession(sessionEntity)
                .join(localizationEntityDao.queryBuilderByKey(key))
                .queryForFirst();
    }

    public QueryBuilder<ResourceEntity, Long> queryBuilderByImportSession(SessionEntity sessionEntity) throws SQLException {
        QueryBuilder<ResourceEntity, Long> qb = queryBuilder();
        qb.where().eq(ResourceEntity.SESSION_ID_FIELD, sessionEntity);
        return qb;
    }

}
