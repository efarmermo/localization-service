package mobi.efarmer.i18n.datamodel.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import mobi.efarmer.i18n.datamodel.entity.IEntity;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 09.12.2014.
 */
public abstract class BaseEntityDao<T extends IEntity, ID> extends BaseDaoImpl<T, ID> {

    protected BaseEntityDao(ConnectionSource connectionSource, Class<T> dataClass) throws SQLException {
        super(connectionSource, dataClass);
        initialize();
    }

    public T findById(Long id) throws SQLException {
        QueryBuilder<T, ID> qb = queryBuilder();
        qb.where().eq(IEntity.ID_FIELD, id);
        return qb.queryForFirst();
    }

}
