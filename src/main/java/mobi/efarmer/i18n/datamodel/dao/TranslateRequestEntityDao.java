package mobi.efarmer.i18n.datamodel.dao;

import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;
import mobi.efarmer.i18n.datamodel.entity.TranslateRequestEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 01.01.15.
 */
@Repository
public class TranslateRequestEntityDao extends BaseEntityDao<TranslateRequestEntity,Long> {

    @Autowired
    public TranslateRequestEntityDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, TranslateRequestEntity.class);
    }

    @Override
    public int create(TranslateRequestEntity data) throws SQLException {
        TranslateRequestEntity entity = queryBuilder().where().eq(TranslateRequestEntity.KEY_FIELD, data.getKey())
                .and().eq(TranslateRequestEntity.APPLICATION_NAME_FIELD, data.getApplicationName())
                .queryForFirst();
        int res = 0;
        if (entity == null) {
            data.setDate(new Date());
            res = super.create(data);
        }
        return res;
    }

    public List<TranslateRequestEntity> allUnprocessed() throws SQLException {
        return queryBuilder().where().eq(TranslateRequestEntity.PROCESSED_FIELD, false).query();
    }

    public boolean processed(Long id) throws SQLException {
        UpdateBuilder<TranslateRequestEntity, Long> ub = updateBuilder();
        ub.updateColumnValue(TranslateRequestEntity.PROCESSED_FIELD, true).where().idEq(id);
        return ub.update() > 0;
    }

    public boolean reject(Long id) throws SQLException {
        return deleteById(id) > 0;
    }

    public boolean rejectByKey(String key) throws SQLException {
        DeleteBuilder db = deleteBuilder();
        db.where().eq(TranslateRequestEntity.KEY_FIELD, key);
        return db.delete() > 0;
    }
}
