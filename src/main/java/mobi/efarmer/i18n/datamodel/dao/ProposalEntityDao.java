package mobi.efarmer.i18n.datamodel.dao;

import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import mobi.efarmer.i18n.datamodel.entity.ProposalEntity;
import mobi.efarmer.i18n.datamodel.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 10.12.2014.
 */
@Repository
public class ProposalEntityDao extends BaseEntityDao<ProposalEntity, Long> {

    @Autowired
    private LocalizationEntityDao localizationEntityDao;

    @Autowired
    public ProposalEntityDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, ProposalEntity.class);
    }

    public List<ProposalEntity> findUnprocessed() throws SQLException {
        return queryBuilder().where().eq(ProposalEntity.PROCESSED_FIELD, false).query();
    }

    @Override
    public int create(ProposalEntity data) throws SQLException {
        int count = 0;
        DatabaseConnection connection = startThreadConnection();
        connection.setAutoCommit(false);
        try {
            localizationEntityDao.create(data.getLocalization());
            count = super.create(data);
            commit(connection);
        } catch (SQLException e) {
            rollBack(connection);
            throw e;
        } finally {
            endThreadConnection(connection);
        }
        return count;
    }

    public int createBatch(List<ProposalEntity> data) throws SQLException {
        return callBatchTasks(() -> {
            int c = 0;
            for (ProposalEntity proposal: data) {
                if (create(proposal) > 0) {
                    c++;
                } else {
                    return 0;
                }
            }
            return c;
        });
    }

    @Override
    public int deleteById(Long id) throws SQLException {
        return delete(findById(id));
    }

    @Override
    public int delete(ProposalEntity data) throws SQLException {
        int count = 0;
        DatabaseConnection connection = startThreadConnection();
        connection.setAutoCommit(false);
        try {
            count = super.delete(data);
            localizationEntityDao.delete(data.getLocalization());
            commit(connection);
        } catch (SQLException e) {
            rollBack(connection);
            throw e;
        } finally {
            endThreadConnection(connection);
        }
        return count;
    }

    public boolean processed(Long id) throws SQLException {
        UpdateBuilder<ProposalEntity, Long> ub = updateBuilder();
        ub.updateColumnValue(ProposalEntity.PROCESSED_FIELD, true).where().idEq(id);
        return ub.update() > 0;
    }

    public List<ProposalEntity> findProposals(UserEntity user, boolean processed) throws SQLException {
        return queryBuilder().where().eq(ProposalEntity.AUTHOR_FIELD, user.getId())
                .and().eq(ProposalEntity.PROCESSED_FIELD, processed).query();
    }

}
