package mobi.efarmer.i18n.datamodel.dao;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import mobi.efarmer.i18n.datamodel.entity.HeadEntity;
import mobi.efarmer.i18n.datamodel.entity.HistoryEntity;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 09.12.2014.
 */
@Repository
public class HistoryEntityDao extends BaseEntityDao<HistoryEntity, Long> {

    @Autowired
    private HeadEntityDao headEntityDao;

    @Autowired
    private LocalizationEntityDao localizationEntityDao;

    @Autowired
    private ProposalEntityDao proposalEntityDao;

    @Autowired
    private TranslateRequestEntityDao translateRequestEntityDao;

    @Autowired
    public HistoryEntityDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, HistoryEntity.class);
    }

    @Override
    public int create(HistoryEntity data) throws SQLException {
        int res = 0;
        DatabaseConnection connection = startThreadConnection();
        connection.setAutoCommit(false);
        try {
            HeadEntity head = headEntityDao.findByKey(data.getKey());
            if (head != null) {
                localizationEntityDao.refresh(head.getLocalization());
                JSONObject i18n = head.getLocalization().getI18n();
                JSONObject editedI18n = data.getLocalization().getI18n();

                for (String key: editedI18n.keySet()) {
                    i18n.put(key, editedI18n.get(key));
                }
                data.getLocalization().setI18n(i18n);

                if (data.getTags() == null) {
                    data.setTags(head.getTags());
                }
            }
            localizationEntityDao.create(data.getLocalization());

            res = super.create(data);
            headEntityDao.setHead(data.getId());

            if (data.getProposal() != null) {
                proposalEntityDao.processed(data.getProposal().getId());
            }
            if (data.getTranslateRequest() != null) {
                translateRequestEntityDao.processed(data.getTranslateRequest().getId());
            }
            commit(connection);
        } catch (SQLException e) {
            rollBack(connection);
            throw new SQLException(data.getKey(), e);
        } finally {
            endThreadConnection(connection);
        }
        return res;
    }

    public int batchCreate(List<HistoryEntity> historyEntities) throws SQLException {
        return callBatchTasks(() -> {
            int res = 0;
            for (HistoryEntity historyEntity: historyEntities) {
                res += create(historyEntity);
            }
            return res;
        });
    }

    public List<HistoryEntity> getHistory(String key) throws SQLException {
        return queryBuilderByKey(key).query();
    }

    public QueryBuilder<HistoryEntity, Long> queryBuilderByKey(String key) throws SQLException {
        QueryBuilder<HistoryEntity, Long> qb = queryBuilder();
        qb.where().eq(HistoryEntity.KEY_FIELD, key);
        qb.orderBy(HistoryEntity.UPDATE_TIMESTAMP_FIELD, false);
        return qb;
    }

}
