package mobi.efarmer.i18n.datamodel.type;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.BaseDataType;
import com.j256.ormlite.support.DatabaseResults;
import org.json.JSONArray;
import org.json.JSONException;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 09.12.2014.
 */
public class JSONArrayDataType extends BaseDataType {

    private static class SinglentonHolder {
        private static final JSONArrayDataType singleton = new JSONArrayDataType();
    }

    public static JSONArrayDataType getSingleton() {
        return SinglentonHolder.singleton;
    }

    public JSONArrayDataType() {
        super(SqlType.OTHER, new Class[]{JSONArray.class});
    }

    @Override
    public Object parseDefaultString(FieldType fieldType, String defaultStr) throws SQLException {
        return createJsonArray(defaultStr);
    }

    @Override
    public Object resultToSqlArg(FieldType fieldType, DatabaseResults results, int columnPos) throws SQLException {
        return createJsonArray(results.getString(columnPos));
    }

    private JSONArray createJsonArray(String jsonString) throws SQLException {
        JSONArray jsonArray = null;
        if (jsonString != null) {
            try {
                jsonArray = new JSONArray(jsonString);
            } catch (JSONException e) {
                throw new SQLException(e);
            }
        }
        return jsonArray;
    }
}
