package mobi.efarmer.i18n.datamodel.type;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.BaseDataType;
import com.j256.ormlite.support.DatabaseResults;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 09.12.2014.
 */
public class JSONObjectDataType extends BaseDataType {

    private static class SinglentonHolder {
        private static final JSONObjectDataType singleton = new JSONObjectDataType();
    }

    public static JSONObjectDataType getSingleton() {
        return SinglentonHolder.singleton;
    }

    public JSONObjectDataType() {
        super(SqlType.OTHER, new Class[]{JSONObject.class});
    }

    @Override
    public JSONObject parseDefaultString(FieldType fieldType, String defaultStr) throws SQLException {
        return createJson(defaultStr);
    }

    @Override
    public Object resultToSqlArg(FieldType fieldType, DatabaseResults results, int columnPos) throws SQLException {
        return createJson(results.getString(columnPos));
    }

    private JSONObject createJson(String jsonString) throws SQLException {
        JSONObject json;
        try {
            json = new JSONObject(jsonString);
        } catch (JSONException e) {
            throw new SQLException(e);
        }
        return json;
    }

}
