package mobi.efarmer.i18n.datamodel.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Maxim Maximchuk
 *         date 03.02.15.
 */
@DatabaseTable(tableName = "user_role")
public class UserRoleEntity implements IEntity {

    public static final String USER_ID_FIELD = "user_id";
    public static final String ROLE_ID_FIELD = "role_id";

    @DatabaseField(generatedIdSequence = "user_role_id_seq")
    private Long id;

    @DatabaseField(columnName = USER_ID_FIELD, foreign = true)
    public UserEntity user;

    @DatabaseField(columnName = ROLE_ID_FIELD, foreign = true, foreignAutoRefresh = true)
    public RoleEntity role;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public RoleEntity getRole() {
        return role;
    }

    public void setRole(RoleEntity role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRoleEntity that = (UserRoleEntity) o;

        if (!role.equals(that.role)) return false;
        if (!user.equals(that.user)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = user.hashCode();
        result = 31 * result + role.hashCode();
        return result;
    }
}
