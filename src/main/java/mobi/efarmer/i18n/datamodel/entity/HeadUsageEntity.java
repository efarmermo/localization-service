package mobi.efarmer.i18n.datamodel.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonParam;
import com.maximchuk.json.exception.JsonException;
import org.json.JSONObject;

/**
 * Created by Maxim Maximchuk
 * date 12-Jan-16.
 */
@DatabaseTable(tableName = "head_usage")
public class HeadUsageEntity extends JsonDTO implements IEntity {

    public static final String APPLICATION_NAME_FIELD = "application_name";
    public static final String APPLICATION_VERSION_FIELD = "application_version";
    public static final String KEY_FIELD = "key";

    @DatabaseField(columnName = ID_FIELD, generatedIdSequence = "history_id_seq")
    private Long id;

    @DatabaseField(columnName = KEY_FIELD)
    private String key;

    @DatabaseField(columnName = APPLICATION_NAME_FIELD)
    @JsonParam(name = "app_name")
    private String applicationName;

    @DatabaseField(columnName = APPLICATION_VERSION_FIELD)
    @JsonParam(name = "app_version")
    private String applicationVersion;

    public HeadUsageEntity() {
    }

    public HeadUsageEntity(JSONObject json) throws JsonException {
        super(json);
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public void setApplicationVersion(String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
