package mobi.efarmer.i18n.datamodel.entity;

import java.io.Serializable;

/**
 * @author Maxim Maximchuk
 *         date 09.12.2014.
 */
public interface IEntity extends Serializable {

    static final String ID_FIELD = "id";
    String JSON_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    Long getId();

}
