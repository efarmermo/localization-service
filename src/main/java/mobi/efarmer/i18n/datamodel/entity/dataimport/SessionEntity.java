package mobi.efarmer.i18n.datamodel.entity.dataimport;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonDateParam;
import com.maximchuk.json.annotation.JsonParam;
import mobi.efarmer.i18n.datamodel.entity.IEntity;
import mobi.efarmer.i18n.datamodel.entity.LocalizationEntity;
import mobi.efarmer.i18n.datamodel.entity.UserEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 17.02.15.
 */
@DatabaseTable(tableName = "import.session")
public class SessionEntity extends JsonDTO implements IEntity {

    public static final String SRC_NAME_FIELD = "src_name";
    public static final String DATE_FIELD = "date";
    public static final String AUTHOR = "author_id";
    public static final String AUTHOR_JSON = "author";

    @DatabaseField(columnName = ID_FIELD, generatedIdSequence = "import.session_id_seq")
    private Long id;

    @DatabaseField(columnName = SRC_NAME_FIELD)
    private String srcName;

    @JsonParam(name = AUTHOR_JSON)
    @DatabaseField(columnName = AUTHOR, foreign = true, foreignAutoRefresh = true)
    private UserEntity author;

    @JsonDateParam(pattern = JSON_DATE_PATTERN)
    @DatabaseField(columnName = DATE_FIELD)
    private Date date;

    private List<LocalizationEntity> localizations;

    private List<Long> selectedIds;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSrcName() {
        return srcName;
    }

    public void setSrcName(String srcName) {
        this.srcName = srcName;
    }

    public UserEntity getAuthor() {
        return author;
    }

    public void setAuthor(UserEntity author) {
        this.author = author;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<LocalizationEntity> getLocalizations() {
        return localizations;
    }

    public void setLocalizations(List<LocalizationEntity> localizations) {
        this.localizations = localizations;
    }

    public List<Long> getSelectedIds() {
        return selectedIds;
    }

    public void setSelectedIds(List<Long> selectedIds) {
        this.selectedIds = selectedIds;
    }

    public void addLocalization(LocalizationEntity localization) {
        if (localizations == null) {
            localizations = new ArrayList<>();
        }
        localizations.add(localization);
    }
}
