package mobi.efarmer.i18n.datamodel.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonIgnore;
import com.maximchuk.oauth.resource.OAuthUser;
import mobi.efarmer.i18n.rest.security.RoleEnum;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 23.12.14.
 */
@DatabaseTable(tableName = "user")
public class UserEntity extends JsonDTO implements OAuthUser, IEntity {

    public static final String USERNAME_FIELD = "username";
    public static final String LAST_REQUEST_DATE_FIELD = "last_request_timestamp";

    @JsonIgnore
    @DatabaseField(generatedIdSequence = "user_id_seq")
    private Long id;

    @DatabaseField(columnName = USERNAME_FIELD)
    private String username;

    @DatabaseField(columnName = LAST_REQUEST_DATE_FIELD)
    private Date lastRequestDate;

    @JsonIgnore
    @ForeignCollectionField
    private Collection<UserRoleEntity> userRoles;

    private List<RoleEnum> roles;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getLastRequestDate() {
        return lastRequestDate;
    }

    public void setLastRequestDate(Date lastRequestDate) {
        this.lastRequestDate = lastRequestDate;
    }

    public Collection<UserRoleEntity> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Collection<UserRoleEntity> userRoles) {
        this.userRoles = userRoles;
    }

    @Override
    public List<RoleEnum> getRoles() {
        if (roles == null) {
            refreshRoles();
        }
        return roles;
    }

    public boolean hasRole(RoleEnum role) {
        boolean exist = false;
        for (UserRoleEntity userRole: userRoles) {
            exist = userRole.getRole().getRoleEnum() == role;
            if (exist) {
                break;
            }
        }
        return exist;
    }

    private void refreshRoles() {
        roles = new ArrayList<>();
        if (userRoles != null) {
            for (UserRoleEntity userRole: userRoles) {
                roles.add(userRole.getRole().getRoleEnum());
            }
        }
    }

}
