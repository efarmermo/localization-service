package mobi.efarmer.i18n.datamodel.entity;

import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonIgnore;
import com.maximchuk.json.annotation.JsonParam;
import com.maximchuk.json.exception.JsonException;
import mobi.efarmer.i18n.datamodel.dao.LocalizationEntityDao;
import mobi.efarmer.i18n.datamodel.dao.ProposalEntityDao;
import mobi.efarmer.i18n.datamodel.dao.TranslateRequestEntityDao;
import org.json.JSONObject;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 10.12.2014.
 */
public class LocalizationEntryDto extends JsonDTO {

    private String key;

    private String note;

    private String tags;

    public JSONObject i18n;

    @JsonParam(name = "proposal")
    public Long proposalId;

    @JsonParam(name = "request")
    public Long translateRequestId;

    @JsonIgnore
    public ProposalEntity proposal;

    @JsonIgnore
    public TranslateRequestEntity translateRequest;

    public LocalizationEntryDto() {
    }

    public LocalizationEntryDto(JSONObject json) throws JsonException {
        super(json);
    }

    public LocalizationEntryDto(HeadEntity head, LocalizationEntityDao localizationEntityDao) throws SQLException {
        localizationEntityDao.refresh(head.getLocalization());
        setI18n(head.getLocalization().getI18n());
        setKey(head.getKey());
        setTags(head.getTags());
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public JSONObject getI18n() {
        return i18n;
    }

    public void setI18n(JSONObject i18n) {
        this.i18n = i18n;
    }

    public Long getProposalId() {
        return proposalId;
    }

    public void setProposalId(Long proposalId) {
        this.proposalId = proposalId;
    }

    public Long getTranslateRequestId() {
        return translateRequestId;
    }

    public void setTranslateRequestId(Long translateRequestId) {
        this.translateRequestId = translateRequestId;
    }

    public ProposalEntity readProposal(ProposalEntityDao proposalEntityDao) throws SQLException {
        if (proposal == null && proposalId != null) {
            proposal = proposalEntityDao.findById(proposalId);
        }
        return proposal;
    }

    public TranslateRequestEntity readTranslateRequest(TranslateRequestEntityDao translateRequestEntityDao) throws SQLException {
        if (translateRequest == null && translateRequestId != null) {
            translateRequest = translateRequestEntityDao.findById(translateRequestId);
        }
        return translateRequest;
    }
}
