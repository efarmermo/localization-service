package mobi.efarmer.i18n.datamodel.entity.dataimport;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximchuk.json.JsonDTO;
import mobi.efarmer.i18n.datamodel.entity.IEntity;
import mobi.efarmer.i18n.datamodel.entity.LocalizationEntity;

/**
 * @author Maxim Maximchuk
 *         date 17.02.15.
 */
@DatabaseTable(tableName = "import.resource")
public class ResourceEntity extends JsonDTO implements IEntity {

    public static final String SESSION_ID_FIELD = "session_id";
    public static final String LOCALIZATION_ID_FIELD = "localization_id";

    @DatabaseField(columnName = ID_FIELD, generatedIdSequence = "import.resource_id_seq")
    private Long id;

    @DatabaseField(columnName = SESSION_ID_FIELD, foreign = true)
    private SessionEntity session;

    @DatabaseField(columnName = LOCALIZATION_ID_FIELD, foreign = true)
    private LocalizationEntity localization;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SessionEntity getSession() {
        return session;
    }

    public void setSession(SessionEntity session) {
        this.session = session;
    }

    public LocalizationEntity getLocalization() {
        return localization;
    }

    public void setLocalization(LocalizationEntity localization) {
        this.localization = localization;
    }
}
