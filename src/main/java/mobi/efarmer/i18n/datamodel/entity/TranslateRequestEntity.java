package mobi.efarmer.i18n.datamodel.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonDateParam;
import com.maximchuk.json.annotation.JsonIgnore;
import com.maximchuk.json.annotation.JsonParam;

import java.util.Date;

/**
 * @author Maxim Maximchuk
 *         date 01.01.15.
 */
@DatabaseTable(tableName = "request")
public class TranslateRequestEntity extends JsonDTO implements IEntity {

    public static final String KEY_FIELD = "key";
    public static final String APPLICATION_NAME_FIELD = "application_name";
    public static final String APPLICATION_VERSION_FIELD = "application_version";
    public static final String CREATE_DATE_FIELD = "create_date";
    public static final String PROCESSED_FIELD = "processed";
    @DatabaseField(columnName = ID_FIELD, generatedIdSequence = "request_id_seq")
    private Long id;

    @DatabaseField(columnName = KEY_FIELD)
    private String key;

    @JsonParam(name = "appName")
    @DatabaseField(columnName = APPLICATION_NAME_FIELD)
    private String applicationName;

    @JsonParam(name = "appVer")
    @DatabaseField(columnName = APPLICATION_VERSION_FIELD)
    private String applicationVersion;

    @JsonDateParam(pattern = JSON_DATE_PATTERN)
    @DatabaseField(columnName = CREATE_DATE_FIELD)
    private Date date;

    @JsonIgnore
    @DatabaseField(columnName = PROCESSED_FIELD)
    private Boolean processed = false;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public void setApplicationVersion(String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

}
