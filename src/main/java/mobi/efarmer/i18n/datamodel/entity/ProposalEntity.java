package mobi.efarmer.i18n.datamodel.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonParam;

import java.util.Date;

/**
 * @author Maxim Maximchuk
 *         date 10.12.2014.
 */
@DatabaseTable(tableName = "proposal")
public class ProposalEntity extends JsonDTO implements IEntity {

    public static final String KEY_FIELD = "key";
    public static final String CREATE_TIMESTAMP_FIELD = "create_timestamp";
    public static final String AUTHOR_FIELD = "author_id";
    public static final String PROCESSED_FIELD = "processed";
    public static final String LOCALIZATION_ID_FIELD = "localization_id";

    public static final String AUTHOR_JSON_FIELD = "author";


    @DatabaseField(columnName = ID_FIELD, generatedIdSequence = "proposal_id_seq")
    private Long id;

    @DatabaseField(columnName = ProposalEntity.KEY_FIELD)
    private String key;

    @DatabaseField(columnName = ProposalEntity.CREATE_TIMESTAMP_FIELD)
    private Date createDate;

    @JsonParam(name = AUTHOR_JSON_FIELD)
    @DatabaseField(foreign = true, columnName = AUTHOR_FIELD)
    private UserEntity author;

    @DatabaseField(columnName = ProposalEntity.PROCESSED_FIELD)
    private Boolean processed = false;

    @DatabaseField(columnName = LOCALIZATION_ID_FIELD, foreign = true)
    private LocalizationEntity localization;

    public ProposalEntity() {
    }

    public ProposalEntity(LocalizationEntryDto localizationEntry, UserEntity author) {
        key = localizationEntry.getKey();
        localization = new LocalizationEntity(localizationEntry);
        createDate = new Date();
        this.author = author;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public UserEntity getAuthor() {
        return author;
    }

    public void setAuthor(UserEntity author) {
        this.author = author;
    }

    public Boolean getProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    public LocalizationEntity getLocalization() {
        return localization;
    }

    public void setLocalization(LocalizationEntity localization) {
        this.localization = localization;
    }
}
