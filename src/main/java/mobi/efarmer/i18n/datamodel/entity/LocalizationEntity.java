package mobi.efarmer.i18n.datamodel.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonIgnore;
import mobi.efarmer.i18n.datamodel.type.JSONObjectDataType;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Maxim Maximchuk
 *         date 09.12.2014.
 */
@DatabaseTable(tableName = "localization")
public class LocalizationEntity extends JsonDTO implements IEntity {
    private static final long serialVersionUID = 2437751571549890751L;

    public static final String KEY_FIELD = "key";
    public static final String I18N_FIELD = "i18n";

    @JsonIgnore
    @DatabaseField(generatedIdSequence = "localization_id_seq")
    private Long id;

    @DatabaseField(columnName = KEY_FIELD)
    private String key;

    @DatabaseField(columnName = LocalizationEntity.I18N_FIELD, persisterClass = JSONObjectDataType.class)
    private JSONObject i18n = new JSONObject();

    public LocalizationEntity() {
    }

    public LocalizationEntity(LocalizationEntryDto localizationEntry) {
        key = localizationEntry.getKey();
        i18n = localizationEntry.getI18n();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key.trim();
    }

    public JSONObject getI18n() {
        return i18n;
    }

    public void setI18n(JSONObject i18n) {
        this.i18n = i18n;
    }

    public void putTranslate(String lang, String value) {
        i18n.put(lang, value);
    }

    public String getTranslate(String lang) {
        try {
            return i18n.getString(lang);
        } catch (JSONException e) {
            return null;
        }
    }
}
