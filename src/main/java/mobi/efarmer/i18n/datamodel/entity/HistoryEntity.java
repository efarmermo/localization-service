package mobi.efarmer.i18n.datamodel.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonDateParam;
import mobi.efarmer.i18n.datamodel.dao.ProposalEntityDao;
import mobi.efarmer.i18n.datamodel.dao.TranslateRequestEntityDao;

import java.sql.SQLException;
import java.util.Date;

/**
 * @author Maxim Maximchuk
 *         date 09.12.2014.
 */
@DatabaseTable(tableName = "history")
public class HistoryEntity extends JsonDTO implements IEntity {

    public static final String KEY_FIELD = "key";
    public static final String NOTE_FILED = "note";
    public static final String TAGS_FIELD = "tags";
    public static final String AUTHOR_FIELD = "author_id";
    public static final String UPDATE_TIMESTAMP_FIELD = "update_timestamp";
    public static final String LOCALIZATION_ID_FIELD = "localization_id";
    public static final String PROPOSAL_ID_FIELD = "proposal_id";

    @DatabaseField(columnName = ID_FIELD, generatedIdSequence = "history_id_seq")
    private Long id;

    @DatabaseField(columnName = KEY_FIELD)
    private String key;

    @DatabaseField(columnName = NOTE_FILED)
    private String note;

    @DatabaseField(columnName = TAGS_FIELD)
    private String tags;

    @DatabaseField(foreign = true, columnName = AUTHOR_FIELD)
    private UserEntity author;

    @JsonDateParam(pattern = JSON_DATE_PATTERN)
    @DatabaseField(columnName = UPDATE_TIMESTAMP_FIELD)
    private Date updateDate;

    @DatabaseField(columnName = LOCALIZATION_ID_FIELD, foreign = true)
    private LocalizationEntity localization;

    @DatabaseField(columnName = PROPOSAL_ID_FIELD, foreign = true)
    private ProposalEntity proposal;

    private TranslateRequestEntity translateRequest;

    public HistoryEntity() {
    }

    public HistoryEntity(LocalizationEntryDto localizationEntry, UserEntity author,
                         ProposalEntityDao proposalEntityDao,
                         TranslateRequestEntityDao translateRequestEntityDao) throws SQLException {
        key = localizationEntry.getKey();
        note = localizationEntry.getNote();
        tags = localizationEntry.getTags();
        proposal = localizationEntry.readProposal(proposalEntityDao);
        translateRequest = localizationEntry.readTranslateRequest(translateRequestEntityDao);
        localization = new LocalizationEntity(localizationEntry);
        updateDate = new Date();
        this.author = author;
    }

    public HistoryEntity(LocalizationEntity localization, UserEntity author) throws SQLException {
        this.key = localization.getKey();
        this.localization = localization;
        this.updateDate = new Date();
        this.author = author;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public UserEntity getAuthor() {
        return author;
    }

    public void setAuthor(UserEntity author) {
        this.author = author;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public LocalizationEntity getLocalization() {
        return localization;
    }

    public void setLocalization(LocalizationEntity localization) {
        this.localization = localization;
    }

    public ProposalEntity getProposal() {
        return proposal;
    }

    public void setProposal(ProposalEntity proposal) {
        this.proposal = proposal;
    }

    public TranslateRequestEntity getTranslateRequest() {
        return translateRequest;
    }

    public void setTranslateRequest(TranslateRequestEntity translateRequest) {
        this.translateRequest = translateRequest;
    }
}
