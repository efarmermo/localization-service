package mobi.efarmer.i18n.datamodel.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonDateParam;

import java.util.Date;

/**
 * @author Maxim Maximchuk
 *         date 09.12.2014.
 */
@DatabaseTable(tableName = "head")
public class HeadEntity extends JsonDTO implements IEntity {

    public static final String KEY_FIELD = "key";
    public static final String TAGS_FIELD = "tags";
    public static final String UPDATE_TIMESTAMP_FIELD = "update_timestamp";
    public static final String LOCALIZATION_ID_FIELD = "localization_id";

    @DatabaseField(columnName = ID_FIELD, id = true)
    private Long id;

    @DatabaseField(columnName = KEY_FIELD)
    private String key;

    @DatabaseField(columnName = TAGS_FIELD)
    private String tags;

    @JsonDateParam(pattern = "yyy-MM-dd'T'HH:mm:ss")
    @DatabaseField(columnName = UPDATE_TIMESTAMP_FIELD)
    private Date updateDate;

    @DatabaseField(foreign = true, columnName = LOCALIZATION_ID_FIELD)
    private LocalizationEntity localization;

    public HeadEntity() {
    }

    public HeadEntity(HistoryEntity historyEntity) {
        this.id = historyEntity.getId();
        this.key = historyEntity.getKey();
        this.tags = historyEntity.getTags();
        this.updateDate = historyEntity.getUpdateDate();
        this.localization = historyEntity.getLocalization();
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public LocalizationEntity getLocalization() {
        return localization;
    }

    public void setLocalization(LocalizationEntity localization) {
        this.localization = localization;
    }

}
