package mobi.efarmer.i18n.datamodel.entity;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import mobi.efarmer.i18n.rest.security.RoleEnum;

/**
 * @author Maxim Maximchuk
 *         date 03.02.15.
 */
@DatabaseTable(tableName = "role")
public class RoleEntity implements IEntity {

    public static final String ROLE_ENUM_FIELD = "role_name";

    @DatabaseField(generatedIdSequence = "role_id_seq")
    private Long id;

    @DatabaseField(columnName = RoleEntity.ROLE_ENUM_FIELD, dataType = DataType.ENUM_STRING)
    private RoleEnum roleEnum;

    public RoleEntity() {
        super();
    }

    public RoleEntity(RoleEnum roleEnum) {
        this.roleEnum = roleEnum;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoleEnum getRoleEnum() {
        return roleEnum;
    }

    public void setRoleEnum(RoleEnum roleEnum) {
        this.roleEnum = roleEnum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleEntity that = (RoleEntity) o;

        return roleEnum == that.roleEnum;

    }

    @Override
    public int hashCode() {
        return roleEnum.hashCode();
    }

    @Override
    public String toString() {
        return roleEnum.toString();
    }
}
