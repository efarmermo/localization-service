package mobi.efarmer.i18n.datamodel;

import com.j256.ormlite.db.PostgresDatabaseType;
import com.j256.ormlite.jdbc.DataSourceConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date    8/8/14.
 */
public class ConnectionSourceFactory {

    private ConnectionSource connectionSource;

    private ConnectionSourceFactory(String jndiName) {
        try {
            DataSource ds = (DataSource) new InitialContext().lookup(jndiName);
            connectionSource = new DataSourceConnectionSource(ds, new PostgresDatabaseType());
        } catch (NamingException | SQLException ex) {
            ex.printStackTrace();
        }
    }

    public ConnectionSource getConnectionSource() {
        return connectionSource;
    }

}
