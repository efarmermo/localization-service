package mobi.efarmer.i18n.util;

import mobi.efarmer.i18n.datamodel.entity.LocalizationEntity;
import mobi.efarmer.i18n.datamodel.entity.dataimport.SessionEntity;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * @author Maxim Maximchuk
 *         date 06.02.15.
 */
public class XmlProcessor {

    private static final String LOCALIZATION_TAG = "localization";
    private static final String RESOURCE_TAG = "resource";
    private static final String TRANSLATE_TAG = "translate";

    private static final String NAME_ATTR = "name";
    private static final String KEY_ATTR = "key";
    private static final String LANG_ATTR = "lang";
    private static final String DATE_ATTR = "date";

    private XmlProcessor() {
        super();
    }

    public static byte[] writeXml(String name, List<LocalizationEntity> localizations) throws ParserConfigurationException, TransformerException {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.newDocument();
        Element rootElement = doc.createElement(LOCALIZATION_TAG);
        rootElement.setAttribute(NAME_ATTR, name);
        rootElement.setAttribute(DATE_ATTR, String.valueOf(new Date().getTime()));
        doc.appendChild(rootElement);

        for (LocalizationEntity localization: localizations) {
            Element i18nElement = doc.createElement(RESOURCE_TAG);
            i18nElement.setAttribute(KEY_ATTR, localization.getKey());

            JSONObject i18n = localization.getI18n();
            for (Object langObj: i18n.keySet()) {
                String lang = (String)langObj;
                Element translateElement = doc.createElement(TRANSLATE_TAG);
                translateElement.setAttribute(LANG_ATTR, lang);
                translateElement.setTextContent(i18n.getString(lang));
                i18nElement.appendChild(translateElement);
            }
            rootElement.appendChild(i18nElement);
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        StreamResult result = new StreamResult(outputStream);
        transformer.transform(new DOMSource(doc), result);
        return outputStream.toByteArray();
    }

    public static SessionEntity readXml(InputStream inputStream) throws XmlParseException {
        try {
            Handler handler = new Handler();
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            XMLReader reader = parser.getXMLReader();
            reader.setContentHandler(handler);
            reader.parse(new InputSource(inputStream));
            return handler.getResult();
        } catch (Exception e) {
            throw new XmlParseException(e);
        }
    }
    public static SessionEntity readXml(String xmlString) throws XmlParseException {
        return readXml(xmlString.getBytes());
    }

    public static SessionEntity readXml( byte[] data) throws XmlParseException {
        return readXml(new ByteArrayInputStream(data));
    }

    private static class Handler extends DefaultHandler {

        private SessionEntity session;
        private LocalizationEntity localization;

        private String lang;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (qName.equals(LOCALIZATION_TAG)) {
                session = new SessionEntity();
                session.setSrcName(attributes.getValue(NAME_ATTR));
                session.setDate(new Date());
            }
            if (qName.equals(RESOURCE_TAG)) {
                localization = new LocalizationEntity();
                localization.setKey(attributes.getValue(KEY_ATTR));
                lang = null;
            } else if (qName.equals(TRANSLATE_TAG)) {
                lang = attributes.getValue(LANG_ATTR);
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (qName.equals(RESOURCE_TAG)) {
                session.addLocalization(localization);
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (lang != null) {
                String value = new String(ch, start, length).trim();
                if (!value.isEmpty()) {
                    String translate = localization.getTranslate(lang);
                    localization.putTranslate(lang, translate != null? translate + value: value);
                }
            }
        }

        public SessionEntity getResult() {
            return session;
        }
    }

}
