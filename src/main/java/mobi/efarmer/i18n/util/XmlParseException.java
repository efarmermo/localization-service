package mobi.efarmer.i18n.util;

/**
 * @author Maxim Maximchuk
 *         date 18.02.15.
 */
public class XmlParseException extends Exception {

    public XmlParseException(Throwable cause) {
        super(cause);
    }

}