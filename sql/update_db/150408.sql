ALTER TABLE history DROP CONSTRAINT fk_request;
ALTER TABLE history DROP COLUMN request_id;

INSERT INTO db_version VALUES ('150408.sql');