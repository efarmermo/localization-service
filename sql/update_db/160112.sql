CREATE TABLE head_usage (
  id                  BIGINT PRIMARY KEY,
  head_id             BIGINT       NOT NULL,
  application_name    VARCHAR(150) NOT NULL,
  application_version VARCHAR(50)  NOT NULL,

  CONSTRAINT head_usage_unq UNIQUE (head_id, application_name, application_version),
  CONSTRAINT fk_head FOREIGN KEY (head_id) REFERENCES head (id)
);
CREATE INDEX head_usage_head_id_idx ON head_usage USING BTREE (head_id);
CREATE INDEX head_usage_application_name_idx ON head_usage USING BTREE (application_name);
CREATE INDEX head_usage_application_version_idx ON head_usage USING BTREE (application_version);

CREATE SEQUENCE head_usage_id_seq INCREMENT BY 10 MINVALUE 1 MAXVALUE 9223372036854775807 START 10 CACHE 1;

INSERT INTO db_version VALUES ('160112.sql');