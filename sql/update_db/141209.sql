CREATE TABLE localization (
  id   BIGINT PRIMARY KEY,
  key  VARCHAR NOT NULL,
  i18n JSON    NOT NULL
);

CREATE TABLE proposal (
  id               BIGINT PRIMARY KEY,
  localization_id  BIGINT      NOT NULL,
  key              VARCHAR(50) NOT NULL,
  create_timestamp TIMESTAMP   NOT NULL,
  author           VARCHAR(128),
  processed        BOOLEAN     NOT NULL DEFAULT FALSE,

  CONSTRAINT fk_localization FOREIGN KEY (localization_id) REFERENCES localization (id)
);

CREATE TABLE history (
  id               BIGINT PRIMARY KEY,
  localization_id  BIGINT      NOT NULL,
  proposal_id      BIGINT,
  author           VARCHAR     NOT NULL,
  key              VARCHAR(50) NOT NULL,
  note             VARCHAR(256),
  tags             JSON,
  update_timestamp TIMESTAMP   NOT NULL,

  CONSTRAINT history_unq UNIQUE (key, update_timestamp),
  CONSTRAINT fk_localization FOREIGN KEY (localization_id) REFERENCES localization (id),
  CONSTRAINT fk_proposal FOREIGN KEY (proposal_id) REFERENCES proposal (id)
);

CREATE TABLE head (
  id               BIGINT PRIMARY KEY,
  localization_id  BIGINT      NOT NULL,
  key              VARCHAR(50) NOT NULL UNIQUE,
  tags             JSON,
  update_timestamp TIMESTAMP   NOT NULL,

  CONSTRAINT fk_history FOREIGN KEY (id) REFERENCES history (id),
  CONSTRAINT fk_localization FOREIGN KEY (localization_id) REFERENCES localization (id)
);

CREATE TABLE db_version (
  ver VARCHAR PRIMARY KEY
);

CREATE INDEX localization_idx ON localization(key);
CREATE INDEX history_idx ON history(key, author, update_timestamp);
CREATE INDEX head_idx ON head(key, update_timestamp);
CREATE INDEX proposal_idx ON proposal(key, processed, create_timestamp);

CREATE SEQUENCE localization_id_seq INCREMENT BY 10 MINVALUE 1 MAXVALUE 9223372036854775807 START 10 CACHE 1;
CREATE SEQUENCE history_id_seq INCREMENT BY 10 MINVALUE 1 MAXVALUE 9223372036854775807 START 10 CACHE 1;
CREATE SEQUENCE proposal_id_seq INCREMENT BY 10 MINVALUE 1 MAXVALUE 9223372036854775807 START 10 CACHE 1;

INSERT INTO db_version VALUES ('141209.sql');