ALTER TABLE request DROP CONSTRAINT request_unq;
DELETE FROM request WHERE rejected = true;
ALTER TABLE request DROP COLUMN rejected;

INSERT INTO db_version VALUES ('150112.sql');