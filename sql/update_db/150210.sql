ALTER TABLE proposal DROP COLUMN author;
ALTER TABLE proposal ADD COLUMN author_id BIGINT NOT NULL;
ALTER TABLE proposal ADD CONSTRAINT fk_author FOREIGN KEY (author_id) REFERENCES "user"(id);

ALTER TABLE history ADD COLUMN author_id BIGINT;
ALTER TABLE history ADD CONSTRAINT fk_author FOREIGN KEY (author_id) REFERENCES "user"(id);
UPDATE history h SET author_id = (SELECT id FROM "user" u where u.username = h.author) WHERE author_id is NULL;
ALTER TABLE history ALTER author_id SET NOT NULL;

ALTER TABLE history DROP COLUMN author;

INSERT INTO db_version VALUES ('150210.sql');