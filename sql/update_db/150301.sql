ALTER TABLE import.session ADD COLUMN author_id BIGINT NOT NULL;
ALTER TABLE import.session ADD
CONSTRAINT fk_user FOREIGN KEY (author_id) REFERENCES public.user(id);

INSERT INTO db_version VALUES ('150301.sql');