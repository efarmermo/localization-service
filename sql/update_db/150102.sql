CREATE TABLE request (
  id               BIGINT PRIMARY KEY,
  key              VARCHAR(15)  NOT NULL,
  application_name VARCHAR(100) NOT NULL,
  create_date      TIMESTAMP    NOT NULL,
  processed        BOOLEAN DEFAULT FALSE,

  CONSTRAINT request_unq UNIQUE (key, application_name)
);

CREATE SEQUENCE request_id_seq INCREMENT BY 10 MINVALUE 1 MAXVALUE 9223372036854775807 START 10 CACHE 1;

ALTER TABLE history ADD COLUMN request_id BIGINT;
ALTER TABLE history ADD CONSTRAINT fk_request FOREIGN KEY (request_id) REFERENCES request (id);

INSERT INTO db_version VALUES ('150102.sql');