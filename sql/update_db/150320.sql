ALTER TABLE request ADD COLUMN application_version VARCHAR(50);
ALTER TABLE request ALTER COLUMN application_name TYPE VARCHAR(100);

INSERT INTO db_version VALUES ('150320.sql')