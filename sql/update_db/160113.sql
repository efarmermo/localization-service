DELETE FROM head_usage;
ALTER TABLE head_usage DROP CONSTRAINT fk_head;
ALTER TABLE head_usage DROP head_id;
ALTER TABLE head_usage ADD COLUMN key VARCHAR(50) NOT NULL;

INSERT INTO db_version VALUES ('160113.sql');